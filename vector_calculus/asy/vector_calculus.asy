// vector_calculus.asy
//  Calculus III material
// 2023-Apr-07 Jim Hefferon GPL

import settings; 
settings.outformat="pdf";
settings.render=0;

import fontsize;
fontsize(9);
// unitsize(1pt);

usepackage("amsmath");

string OUTPUT_FN = "vector_calculus%03d";

defaultpen(0.4); 

import graph;


void draw_vector(picture p, pair v, pair point_at) {
  draw(p, point_at--(point_at+v), blue, Arrow(3));
  dot(p, point_at, blue, NoFill);
  }


// ===== Vector fields
int picnum = 0;
picture pic;

unitsize(pic,0.5cm,0);

draw_vector(pic, (-4,-2), (-2,2));
draw_vector(pic, (-2,-2), (-1,2));
draw_vector(pic, (0,-2), (0,2));
draw_vector(pic, (2,-2), (1,2));
draw_vector(pic, (4,-2), (2,2));

draw_vector(pic, (-4,-1), (-2,1));
draw_vector(pic, (-2,-1), (-1,1));
draw_vector(pic, (0,-1), (0,1));
draw_vector(pic, (2,-1), (1,1));
draw_vector(pic, (4,-1), (2,1));

draw_vector(pic, (-4,0), (-2,0));
draw_vector(pic, (-2,0), (-1,0));
draw_vector(pic, (0,0), (0,0));
draw_vector(pic, (2,0), (1,0));
draw_vector(pic, (4,0), (2,0));

draw_vector(pic, (-4,1), (-2,-1));
draw_vector(pic, (-2,1), (-1,-1));
draw_vector(pic, (0,1), (0,-1));
draw_vector(pic, (2,1), (1,-1));
draw_vector(pic, (4,1), (2,-1));

draw_vector(pic, (-4,2), (-2,-2));
draw_vector(pic, (-2,2), (-1,-2));
draw_vector(pic, (0,2), (0,-2));
draw_vector(pic, (2,2), (1,-2));
draw_vector(pic, (4,2), (2,-2));

xaxis(pic,"", // "$x$",
      xmin=-2.25,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"", // "$y$",
      ymin=-2.25,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........................
int picnum = 1;
picture pic;

unitsize(pic,0.5cm,0);

draw_vector(pic, (-2,2), (-2,2));
draw_vector(pic, (-1,2), (-1,2));
draw_vector(pic, (0,2), (0,2));
draw_vector(pic, (1,2), (1,2));
draw_vector(pic, (2,2), (2,2));

draw_vector(pic, (-2,1), (-2,1));
draw_vector(pic, (-1,1), (-1,1));
draw_vector(pic, (0,1), (0,1));
draw_vector(pic, (1,1), (1,1));
draw_vector(pic, (2,1), (2,1));

draw_vector(pic, (-2,0), (-2,0));
draw_vector(pic, (-1,0), (-1,0));
draw_vector(pic, (0,0), (0,0));
draw_vector(pic, (1,0), (1,0));
draw_vector(pic, (2,0), (2,0));

draw_vector(pic, (-2,-1), (-2,-1));
draw_vector(pic, (-1,-1), (-1,-1));
draw_vector(pic, (0,-1), (0,-1));
draw_vector(pic, (1,-1), (1,-1));
draw_vector(pic, (2,-1), (2,-1));

draw_vector(pic, (-2,-2), (-2,-2));
draw_vector(pic, (-1,-2), (-1,-2));
draw_vector(pic, (0,-2), (0,-2));
draw_vector(pic, (1,-2), (1,-2));
draw_vector(pic, (2,-2), (2,-2));

xaxis(pic,"", // "$x$",
      xmin=-2.25,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"", // "$y$",
      ymin=-2.25,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........................
int picnum = 2;
picture pic;

unitsize(pic,0.5cm,0);

draw_vector(pic, (-2,-2), (-2,2));
draw_vector(pic, (-2,-1), (-1,2));
draw_vector(pic, (-2,0), (0,2));
draw_vector(pic, (-2,1), (1,2));
draw_vector(pic, (-2,2), (2,2));

draw_vector(pic, (-1,-2), (-2,1));
draw_vector(pic, (-1,-1), (-1,1));
draw_vector(pic, (-1,0), (0,1));
draw_vector(pic, (-1,1), (1,1));
draw_vector(pic, (-1,2), (2,1));

draw_vector(pic, (-2,-2), (-2,0));
draw_vector(pic, (-1,-1), (-1,0));
draw_vector(pic, (0,0), (0,0));
draw_vector(pic, (1,1), (1,0));
draw_vector(pic, (2,2), (2,0));

draw_vector(pic, (-2,-2), (-2,-1));
draw_vector(pic, (-1,-1), (-1,-1));
draw_vector(pic, (0,0), (0,-1));
draw_vector(pic, (1,1), (1,-1));
draw_vector(pic, (2,2), (2,-1));

draw_vector(pic, (-2,-2), (-2,-2));
draw_vector(pic, (-1,-1), (-1,-2));
draw_vector(pic, (0,0), (0,-2));
draw_vector(pic, (1,1), (1,-2));
draw_vector(pic, (2,2), (2,-2));

xaxis(pic,"", // "$x$",
      xmin=-2.25,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0)),  // "\tiny $%.2g$"
      Arrows(TeXHead));
yaxis(pic,"", // "$y$",
      ymin=-2.25,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ........................
int picnum = 3;
picture pic;

unitsize(pic,0.5cm,0);

draw_vector(pic, (2,0), (2,0));  // the vector, and the point at
draw_vector(pic, (sqrt(2),sqrt(2)), (sqrt(2),sqrt(2)));
draw_vector(pic, (0,2), (0,2));
draw_vector(pic, (-sqrt(2),sqrt(2)), (-sqrt(2),sqrt(2)));
draw_vector(pic, (-2,0), (-2,0));
draw_vector(pic, (-sqrt(2),-sqrt(2)), (-sqrt(2),-sqrt(2)));
draw_vector(pic, (0,-2), (0,-2));
draw_vector(pic, (sqrt(2),-sqrt(2)), (sqrt(2),-sqrt(2)));

draw_vector(pic, (1,0), (1,0));  // the vector, and the point at
draw_vector(pic, (sqrt(2)/2,sqrt(2)/2), (sqrt(2)/2,sqrt(2)/2));
draw_vector(pic, (0,1), (0,1));
draw_vector(pic, (-sqrt(2)/2,sqrt(2)/2), (-sqrt(2)/2,sqrt(2)/2));
draw_vector(pic, (-1,0), (-1,0));
draw_vector(pic, (-sqrt(2)/2,-sqrt(2)/2), (-sqrt(2)/2,-sqrt(2)/2));
draw_vector(pic, (0,-1), (0,-1));
draw_vector(pic, (sqrt(2)/2,-sqrt(2)/2), (sqrt(2)/2,-sqrt(2)/2));

draw_vector(pic, (0,0), (0,0));

xaxis(pic,"", // "$x$",
      xmin=-2.75,xmax=2.75,
      RightTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"", // "$y$",
      ymin=-2.75,ymax=2.75,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ........................
int picnum = 4;
picture pic;

unitsize(pic,0.5cm,0);

draw_vector(pic, (0,2), (2,0));  // the vector, and the point at
draw_vector(pic, (-sqrt(2),sqrt(2)), (sqrt(2),sqrt(2)));
draw_vector(pic, (-2,0), (0,2));
draw_vector(pic, (-sqrt(2),-sqrt(2)), (-sqrt(2),sqrt(2)));
draw_vector(pic, (0,-2), (-2,0));
draw_vector(pic, (sqrt(2),-sqrt(2)), (-sqrt(2),-sqrt(2)));
draw_vector(pic, (2,0), (0,-2));
draw_vector(pic, (sqrt(2),sqrt(2)), (sqrt(2),-sqrt(2)));

draw_vector(pic, (0,1), (1,0));  // the vector, and the point at
draw_vector(pic, (-sqrt(2)/2,sqrt(2)/2), (sqrt(2)/2,sqrt(2)/2));
draw_vector(pic, (-1,0), (0,1));
draw_vector(pic, (-sqrt(2)/2,-sqrt(2)/2), (-sqrt(2)/2,sqrt(2)/2));
draw_vector(pic, (0,-1), (-1,0));
draw_vector(pic, (sqrt(2)/2,-sqrt(2)/2), (-sqrt(2)/2,-sqrt(2)/2));
draw_vector(pic, (1,0), (0,-1));
draw_vector(pic, (sqrt(2)/2,sqrt(2)/2), (sqrt(2)/2,-sqrt(2)/2));

draw_vector(pic, (0,0), (0,0));

xaxis(pic,"", // "$x$",
      xmin=-2.5,xmax=2.5,
      RightTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"", // "$y$",
      ymin=-2.5,ymax=2.5,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ===============================
// VT barometric pressure map

real prob_density(real x, real y) {
  return abs(x-2*y^2);
}


real level_curve(real x) {
  return sqrt(0.5*x);
}


int picnum = 5;
picture pic;

size(pic,3cm,0);
real width = 3cm;

label(pic, shift(2cm,2.5cm)*graphic("vt.eps","width=3cm, bb=295 165 425 375"),(0,0));
layer(pic);

dotfactor=1;

int dot_grid = 100;
srand(1);  // seed is 1
for(int x=0; x < dot_grid; ++x) {
  for (int y=0; y<dot_grid; ++y) {
    real scaled_x = x/dot_grid;
    real scaled_y = y/dot_grid;
    if (unitrand() < prob_density(scaled_x,scaled_y)) {
      dot(pic, (width*scaled_x+0.5*unitrand(),1.2*width*scaled_y+0.5*unitrand()));
    }
  }
}

// layer(pic);
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ===============================
// VT topographic map
int picnum = 6;
picture pic;

size(pic,3cm,0);
real width = 3cm;
real height = 0.67*width;

path border = (0,0)--(width,0)--(width,height)--(0,height)--cycle;

path one = (0.3*width,height){down}..(0.45*width,0.6*height){down}
  ..(0.32*width,0.45*height)..(0.325*width,0);
path two = (0.5*width,0.3*height){up}..(0.6*width,0.5*height){right}
  ..(0.85*width,0.45*height)..(0.65*width,0.20*height)..cycle;
path three = (0.65*width,0.45*height){up}..(0.7*width,0.475*height){right}
  ..(0.8*width,0.45*height)..(0.75*width,0.30*height)..cycle;


draw(pic,border);
draw(pic,one);
label(pic,"\tiny $100$",point(one,1.5),UnFill);
draw(pic,two);
label(pic,"\tiny $200$",point(two,0.25),UnFill);
draw(pic,three);
label(pic,"\tiny $300$",point(three,3.5),UnFill);
shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// First vector field
int picnum = 7;
picture pic;

unitsize(pic,0.5cm,0);

draw_vector(pic, (1,-2), (-2,2));
draw_vector(pic, (1,-1), (-1,2));
draw_vector(pic, (1,0), (0,2));
draw_vector(pic, (1,1), (1,2));
draw_vector(pic, (1,2), (2,2));

draw_vector(pic, (1,-2), (-2,1));
draw_vector(pic, (1,-1), (-1,1));
draw_vector(pic, (1,0), (0,1));
draw_vector(pic, (1,1), (1,1));
draw_vector(pic, (1,2), (2,1));

draw_vector(pic, (1,-2), (-2,0));
draw_vector(pic, (1,-1), (-1,0));
draw_vector(pic, (1,0), (0,0));
draw_vector(pic, (1,1), (1,0));
draw_vector(pic, (1,2), (2,0));

draw_vector(pic, (1,-2), (-2,-1));
draw_vector(pic, (1,-1), (-1,-1));
draw_vector(pic, (1,0), (0,-1));
draw_vector(pic, (1,1), (1,-1));
draw_vector(pic, (1,2), (2,-1));

draw_vector(pic, (1,-2), (-2,-2));
draw_vector(pic, (1,-1), (-1,-2));
draw_vector(pic, (1,0), (0,-2));
draw_vector(pic, (1,1), (1,-2));
draw_vector(pic, (1,2), (2,-2));

xaxis(pic,"", // "$x$",
      xmin=-2.25,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"", // "$y$",
      ymin=-2.25,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");




// ======================================
// Work done against a spring

// from Bowman's spring.asy
pair coilpoint(real lambda, real r, real t)
{
  return (2.0*lambda*t+r*cos(t),r*sin(t));
}

guide coil(guide g=nullpath, real lambda, real r, real a, real b, int n)
{
  real width=(b-a)/n;
  for(int i=0; i <= n; ++i) {
    real t=a+width*i;
    g=g..coilpoint(lambda,r,t);
  }
  return g;
}

path spring(real x) {
  real r=8;
  real t1=-pi;
  real t2=10*pi;
  real lambda=(t2-t1+x)/(t2-t1);
  pair b=coilpoint(lambda,r,t1);
  pair c=coilpoint(lambda,r,t2);
  pair a=b-20;
  pair d=c+20;

  // draw(pic, a--b,BeginBar(2*barsize()));
  // draw(pic, a--b);
  // draw(pic, c--d);
  // draw(pic, coil(lambda,r,t1,t2,100));
  // dot(pic, d);
  path s = a--b & coil(lambda,r,t1,t2,100) & c--d;
  return s; 
  
  // pair h=20*I;
  // draw(pic, label,a-h--d-h,red,Arrow,Bars,PenMargin);
}


int picnum = 8;
picture pic;

size(pic,3cm,0);

path sp = spring(5);
pair spring_start = point(sp,0);
pair spring_end = point(sp,102);
draw(pic,sp,blue);
// write(format("spring_start is %f\n",spring_start.x));
path wall = (0,-10)--(-5,-10)--(-5,10)--(0,10)--cycle;
fill(pic,shift(spring_start)*wall,gray(0.7));
path bob = (0,-5)--(5,-5)--(5,5)--(0,5)--cycle;
fill(pic,shift(spring_end)*bob,blue);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ====== line integral ========
int picnum = 9;
picture pic;

size(pic,2cm,0);

path C = (0.5,0.5)..(0.4,1.25){up}..(1,2)..(1.7,1.5)..(2.1,1.6);
draw(pic,C,red,MidArcArrow(2));
label(pic,"\footnotesize$C$",point(C,3.5),SW);

xaxis(pic,"\tiny$x$",
      xmin=-0.75,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.75,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ........................................
int picnum = 10;
picture pic;

size(pic,2cm,0);

path C = (0.5,0.5)..(0.4,1.25){up}..(1,2)..(1.7,1.5)..(2.1,1.6);
draw(pic,C,red);
// label(pic,"$C$",point(C,3.5),SW);

draw_vector(pic, (0.5,0.05), (0,0));
draw_vector(pic, (0.5,-0.05), (0,1));
draw_vector(pic, (0.5,0.05),  (0,2));

draw_vector(pic, (0.5,0.15), (1,0));
draw_vector(pic, (0.5,0.25), (1,1));
draw_vector(pic, (0.5,0.15),  (1,2));

draw_vector(pic, (0.5,0.25), (2,0));
draw_vector(pic, (0.5,0.15), (2,1));
draw_vector(pic, (0.5,0.05),  (2,2));

xaxis(pic,"\tiny$x$",
      xmin=-0.75,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.75,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... parabolic path ..................
int picnum = 11;
picture pic;

size(pic,2cm,0);

real f(real x) {return x^2;}
draw(pic, graph(f,0.0,1.00,operator ..),red);


xaxis(pic,"\tiny$x$",
      xmin=-0.75,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.75,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// .......... F(x,y) = scalar .....
// int picnum = 12;
// picture pic;

// size(pic,2cm,0);
// real f(real x) {return x^2;}
// real prob(real x, real y) {
//   return abs(1.5-(y+x))/12; }

// dotfactor=1;

// int dot_grid = 100;
// srand(1);  // seed is 1
// for(int x=0; x < dot_grid; ++x) {
//   for (int y=0; y<dot_grid; ++y) {
//     real scaled_x = 1.75*x/dot_grid;
//     real scaled_y = 1.75*y/dot_grid;
//     if (unitrand() < prob(scaled_x,scaled_y)) {
//       dot(pic, (scaled_x+0.5*unitrand(),scaled_y+0.5*unitrand()), blue);
//     }
//   }
// }

// path C = (0.5,0.5)..(0.4,1.25){up}..(1,2)..(1.7,1.5)..(2.1,1.6);
// draw(pic,C,red);
// // draw(pic, graph(f,0.0,1.00,operator ..),red);

// xaxis(pic,"\tiny$x$",
//       xmin=-0.75,xmax=2.25,
//       RightTicks("%",Step=1,OmitTick(0)),
//       Arrows(TeXHead));
// yaxis(pic,"\tiny$y$",
//       ymin=-0.75,ymax=2.25,
//       LeftTicks("%",Step=1,OmitTick(0)),
//       Arrows(TeXHead));

// shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


import graph3;

// .......... F(x,y) = scalar .....

real prob(real x, real y) {
  return abs(1.5-(y+x))/12; }

int picnum = 12;
picture pic;

size(pic,2cm);
size3(pic,2cm,IgnoreAspect);
currentprojection=perspective(5,2,0.75);

path3 C = (0.5,0.5, prob(0.5,0.5))
  ..(0.4,1.25,prob(0.4,1.25)){(0,1,0)}
  ..(1,2,prob(1,2))
  ..(1.7,1.5,prob(1.7,1.5))
  ..(2.1,1.6,prob(2.1,1.6));
path3 C_base = (0.5,0.5, 0)
  ..(0.4,1.25,0){(0,1,0)}
  ..(1,2,0)
  ..(1.7,1.5,0)
  ..(2.1,1.6,0);

int num_vert_lines=40;
for(int i=0; i<num_vert_lines; ++i) {
  draw(pic, point(C,5*i/num_vert_lines)--point(C_base,5*i/num_vert_lines), gray(0.9));
}
draw(pic,C_base,red);
draw(pic,C,gray(0.7));


axes3(pic,"{\tiny$x$}", "\tiny$y$", "\tiny$F(x,y)$",
      (-0.75,-0.75,-0.25),(2.25,2.25,0.75)
      );

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// .................... dots ..........

real prob(real x, real y) {
  return abs(1.5-(y+x))/12; }

int picnum = 13;
picture pic;

size(pic,2cm,0);

dotfactor=1;

int dot_grid = 100;
srand(1);  // seed is 1
for(int x=0; x < dot_grid; ++x) {
  for (int y=0; y<dot_grid; ++y) {
    real scaled_x = 2*x/dot_grid;
    real scaled_y = 2*y/dot_grid;
    if (unitrand() < prob(scaled_x,scaled_y)) {
      dot(pic, (scaled_x+0.5*unitrand(),scaled_y+0.5*unitrand()), blue);
    }
  }
}

path C = (0.5,0.5)..(0.4,1.25){up}..(1,2)..(1.7,1.5)..(2.1,1.6);
draw(pic,C,red);
// label(pic,"$C$",point(C,3.5),SW);

xaxis(pic,"\tiny$x$",
      xmin=-0.75,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.75,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ======== parabolic path dvided into three parts =======
int picnum = 14;
picture pic;

size(pic,2cm,0);

real f(real x) {return x^2;}
draw(pic, graph(f,0.0,1.00,operator ..),red);

dotfactor = 3; 
dot(pic,(0,f(0)),red);
dot(pic,(0.33,f(0.33)),red);
dot(pic,(0.67,f(0.67)),red);
dot(pic,(1.00,f(1.00)),red);
label(pic, "\tiny$s_1$", (1/6,f(1/6)), N);
label(pic, "\tiny$s_2$", (3/6,f(3/6)), E);
label(pic, "\tiny$s_3$", (5/6,f(5/6)), E);

xaxis(pic,"\tiny$x$",
      xmin=-0.75,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.75,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ======== Defn of line integral of vector fields =======
int picnum = 15;
picture pic;

size(pic,2cm,0);

draw_vector(pic, (0,0.5), (0.5,0.65));
draw_vector(pic, (0,0.5), (1.5,0.65));

draw_vector(pic, (0,0.5), (0,1));
draw_vector(pic, (0,0.5), (1,1));
draw_vector(pic, (0,0.5), (2,1));

draw_vector(pic, (0,0.5), (0.5,1.35));
draw_vector(pic, (0,0.5), (1.5,1.35));

path C = (0,1)--(2,1);
draw(pic, C, red, Arrow(2,Relative(0.4)));
label(pic,"\footnotesize$C$",point(C,0.0),W);

xaxis(pic,"\tiny$x$",
      xmin=-0.75,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.75,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");



// ........... projection of b onto a ..........
int picnum = 16;
picture pic;

size(pic,2cm,0);

path b = (0,0)--(1,1.5);
path a = (0,0)--(2,1);
real dp = 3.5/5;  // a dot b / length(a)^2
path proj = (0,0)--dp*(2,1);

draw(pic, point(b,1)--point(proj,1), dotted);
draw_vector(pic, (2,1), (0,0));  // a
draw_vector(pic, (1,1.5), (0,0));  // b
draw(pic, proj, red, Arrow(3));

label(pic, "\footnotesize$\mathbf{u}$", point(a,0.9),S);
label(pic, "\footnotesize$\mathbf{b}$", point(b,0.75),NW);

xaxis(pic,"\tiny$x$",
      xmin=-0.75,xmax=2.25,
      RightTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.75,ymax=2.25,
      LeftTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ..........parametrization examples ......
int picnum = 17;
picture pic;

size(pic,2cm,0);

path C = (0,0)--(4,4);
draw(pic, C, red);  // , Arrow(2,Relative(0.4)));
// label(pic,"\footnotesize$C$",point(C,0.0),W);

dot(pic, (0,0), red, UnFill);
dot(pic, (1,1), red, UnFill);
dot(pic, (2,2), red, UnFill);
dot(pic, (3,3), red, UnFill);
dot(pic, (4,4), red, UnFill);

label(pic, "\tiny$t=1$", (1,1), 0.5*SE);
label(pic, "\tiny$t=2$", (2,2), 0.5*SE);
label(pic, "\tiny$t=3$", (3,3), 0.5*SE);
label(pic, "\tiny$t=4$", (4,4), 0.75*W);

xaxis(pic,"\tiny$x$",
      xmin=-0.5,xmax=4.75,
      RightTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.5,ymax=4.75,
      LeftTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...........r(t)= (t^2,t^2).........
int picnum = 18;
picture pic;

size(pic,2cm,0);

path C = (0,0)--(4,4);
draw(pic, C, red); 
// label(pic,"\footnotesize$C$",point(C,0.0),W);

dot(pic, (0,0), red, UnFill);
dot(pic, (1,1), red, UnFill);
dot(pic, (4,4), red, UnFill);

label(pic, "\tiny$t=1$", (1,1), 0.5*SE);
label(pic, "\tiny$t=2$", (4,4), 0.75*W);

xaxis(pic,"\tiny$x$",
      xmin=-0.5,xmax=4.75,
      RightTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.5,ymax=4.75,
      LeftTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ...........r(t)= (sqrt(2)t/2,sqrt(2)t/2).........
real s(real x) {return sqrt(2)*x/2;}

int picnum = 19;
picture pic;

size(pic,2cm,0);

path C = (0,0)--(4,4);
draw(pic, C, red); 

dot(pic, (0,0), red, UnFill);
dot(pic, (s(1),s(1)), red, UnFill);
dot(pic, (s(2),s(2)), red, UnFill);
dot(pic, (s(3),s(3)), red, UnFill);
dot(pic, (s(4),s(4)), red, UnFill);
dot(pic, (s(5),s(5)), red, UnFill);

label(pic, "\tiny$t=1$", (s(1),s(1)), 0.5*SE);
label(pic, "\tiny$t=2$", (s(2),s(2)), 0.5*SE);
label(pic, "\tiny$t=3$", (s(3),s(3)), 0.5*SE);
label(pic, "\tiny$t=4$", (s(4),s(4)), 0.75*W);
label(pic, "\tiny$t=5$", (s(5),s(5)), 0.75*W);

xaxis(pic,"\tiny$x$",
      xmin=-0.5,xmax=4.75,
      RightTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.5,ymax=4.75,
      LeftTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

// sage: 8/sqrt(2).n()
// 5.65685424949238




// ======= closed curve in R^3 ============

import three;
import graph3;

// Draw dot with open center
// CS, from https://tex.stackexchange.com/a/321818/339
// void opendot(picture pic=currentpicture, triple v, material p=currentpen,
//          light light=nolight, string name="", render render=defaultrender)
// {
//   pen q=(pen) p;
//   pen fillpen = light.background;
//   if (invisible(fillpen)) fillpen = currentlight.background;
//   if (invisible(fillpen)) fillpen = white;
//   real size=0.5*linewidth(dotsize(q)+q);
//   pic.add(new void(frame f, transform3 t, picture pic, projection P) {
//       triple V=t*v;
//       assert(!is3D(), "opendot() not supported unless settings.prc == false and settings.render != 0");
//       if(pic != null)
//         dot(pic,project(V,P.t),filltype=FillDraw(fillpen=fillpen, drawpen=q));
//     },true);
//   triple R=size*(1,1,1);
//   pic.addBox(v,v,-R,R);
// }

// void opendot(picture pic=currentpicture, Label L, triple v, align align=NoAlign,
//              string format=defaultformat, material p=currentpen,
//              light light=nolight, string name="", render render=defaultrender)
// {
//   Label L=L.copy();
//   if(L.s == "") {
//     if(format == "") format=defaultformat;
//     L.s="("+format(format,v.x)+","+format(format,v.y)+","+
//       format(format,v.z)+")";
//   }
//   L.align(align,E);
//   L.p((pen) p);
//   opendot(pic,v,p,light,name,render);
//   label(pic,L,v,render);
// }


int picnum = 20;
picture pic;

size3(pic,2cm);
currentprojection=orthographic(1,0.75,0.25);

path3 C = (1,1,1){(0,1,1)}..(-1,2,1.5)..(0,2.5,1)..(2,1.5,0.5)..cycle;
draw(pic, C, red, Arrow3(2,Relative(0.5))); 

// dotfactor=3;
// dot(pic,point(C,0.0),red);
// opendot(pic,point(C,0.0),red); lot of errors; physical-based rendering?

label(pic, "\tiny$C$", point(C,1.0), NW);
// label(pic, "\tiny start/end", point(C,0.0), E);

xaxis3(pic,"\tiny$x$",
       xmin=-0.75,xmax=2.5,
       Arrows3(TeXHead3));
yaxis3(pic,"\tiny$y$",
       ymin=-0.75,ymax=2.5,
      Arrows3(TeXHead3));
zaxis3(pic,"\tiny$z$",
       zmin=-0.75,zmax=2.5,
      Arrows3(TeXHead3));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ===== Divergence and curl =======


// ....... divergence ........
int picnum = 21;
picture pic;

size3(pic,2cm);
currentprojection=orthographic(2,0.5,0.25);

triple c0 = (0,0,0);
triple c1 = (0,0,1);
triple c2 = (0,1,0);
triple c3 = (0,1,1);
triple c4 = (1,0,0);
triple c5 = (1,0,1);
triple c6 = (1,1,0);
triple c7 = (1,1,1);

draw(pic, c7--c3);
draw(pic, c5--c1);
draw(pic, c3--c1);
draw(pic, c3--c2);
draw(pic, c2--c6);
draw(pic, c0--c4, dashed);
draw(pic, c0--c2, dashed);
draw(pic, c0--c1, dashed);

path3 flux = (0.5,-0.25,0.5)--(0.5,1.25,0.5); 
draw(pic, flux, white+linewidth(0.8), Arrow3(3));
draw(pic,flux, blue+linewidth(0.4), Arrow3(3));
path3 front = c4--c6--c7--c5--cycle;
draw(pic, front, linewidth(0.8)+white);
draw(pic, front, linewidth(0.4)+black);
draw(pic, c7--c3);
draw(pic, c5--c1);
draw(pic, c3--c1);
draw(pic, c2--c6);

label(pic, "\tiny$\Delta x$", point(c6--c2, 0.5), E);
label(pic, "\tiny$\Delta y$", point(c4--c6, 0.5), S);
label(pic, "\tiny$\Delta z$", point(c2--c3, 0.80), E);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ....... divergence intuition in 2D ........
int picnum = 22;
picture pic;

size(pic,2cm);

path box = (1,1)--(2,1)--(2,2)--(1,2)--cycle;
draw(pic, box);

draw_vector(pic, (0.4,0.3), (1,0.8));
draw_vector(pic, (0.4,0.3), (1.5,0.7));
draw_vector(pic, (0.4,0.3), (0.9,1.2));
draw_vector(pic, (0.7,0.6), (1.1,1.9));
draw_vector(pic, (0.7,0.6), (1.5,1.8));
draw_vector(pic, (0.7,0.6), (1.9,1.7));
draw_vector(pic, (0.6,0.5), (1.8,1.3));

label(pic, "\tiny$\Delta x$", (1,1.5), 2*W);
label(pic, "\tiny$\Delta y$", (1.5,1), 2*S);

// xaxis(pic,"\tiny$x$",
//       xmin=-0.75,xmax=2.25,
//       NoTicks,
//       Arrows(TeXHead));
// yaxis(pic,"\tiny$y$",
//       ymin=-0.75,ymax=2.25,
//       NoTicks,
//       Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");





// ....... curl intuition in 2D ........
int picnum = 23;
picture pic;

unitsize(pic,0.75cm);

path C1 = (0,0)--(2,0);
path C2 = (2,0)--(2,2);
path C3 = (2,2)--(0,2);
path C4 = (0,2)--(0,0);

dotfactor=3;
dot(pic, (0,0), filltype=FillDraw(fillpen=white, drawpen=blue));
dot(pic, (2,0), filltype=FillDraw(fillpen=white, drawpen=blue));
dot(pic, (2,2), filltype=FillDraw(fillpen=white, drawpen=blue));
dot(pic, (0,2), filltype=FillDraw(fillpen=white, drawpen=blue));

draw(pic, C1, blue, MidArrow(3));
  label(pic, "\tiny$C_1$", point(C1,0.3), N);
draw(pic, C2, blue, MidArrow(3));
  label(pic, "\tiny$C_2$", point(C2,0.3), W);
draw(pic, C3, blue, MidArrow(3));
  label(pic, "\tiny$C_3$", point(C3,0.3), S);
draw(pic, C4, blue, MidArrow(3));
  label(pic, "\tiny$C_4$", point(C4,0.3), E);

label(pic, "\tiny$(x,y)$", (0,0), SW);
label(pic, "\tiny$(x+\Delta x,y)$", (2,0), SE);
label(pic, "\tiny$(x+\Delta x,y+\Delta y)$", (2,2), NE);
label(pic, "\tiny$(x,y+\Delta y)$", (0,2), NW);

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........ F(x,y)=(0,x) .......
int picnum = 24;
picture pic;

size(pic,2.5cm,0);

dotfactor=1;

int dot_grid = 6;
for(int x=0; x < dot_grid; ++x) {
  for (int y=0; y<dot_grid; ++y) {
    real scaled_x = 2*x/dot_grid;
    real scaled_y = 2*y/dot_grid;
    draw_vector(pic, (0,scaled_x/5), (scaled_x,scaled_y));
  }
}

path C = circle((1,1),0.4);
draw(pic,C,red,Arrow(3, Relative(0.2)));
// label(pic,"$C$",point(C,3.5),SW);

xaxis(pic,"\tiny$x$",
      xmin=-0.5,xmax=2.25,
      NoTicks,
      // RightTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));
yaxis(pic,"\tiny$y$",
      ymin=-0.5,ymax=2.25,
      NoTicks,
      // LeftTicks("%",Step=1,OmitTick(0),Size=1.25,size=0.75),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

