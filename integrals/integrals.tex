\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}

% \usepackage[perpage,symbol*,para,multiple]{footmisc}
% Getting undefined commands in aux file
\makeatletter
\newcommand\FN@pp@footnote@aux[2]{\relax}
\makeatother
\usepackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 

% set the fonts
\usefonttheme{professionalfonts} % using non standard fonts for beamer
\usefonttheme{serif} % default family is serif
\usepackage{../../presentation,../../presentationfonts}
% \usepackage{jh}
% \newcommand{\colvec}[1]{%
%   \begin{pmatrix} #1 \end{pmatrix}}
\usepackage{../calciii}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Integrals}

\author{Jim Hef{}feron}
\institute{
  Mathematics and Statistics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{hefferon.net}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Integrals}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}




% ------------------------------------------------------
\begin{frame}
  \frametitle{Integration}

Earlier in this class we considered derivatives in Calc~I
$\map{f}{\R}{\R}$ 
and generalized it
to higher dimensions, $\map{f}{\R^2}{\R}$ or $\map{f}{\R^n}{\R}$.
That gave us insight into what Calc~I derivatives
are really about. 

The same happens with integration.
In Calculus~I and~II we learn integration as the area under the curve.
As with differentiation this proves to be not wrong, 
but not the single best way to think about it.

\pause
Start by thinking about grade school arithmetic.
There, if you begin with 3 apples and
get 4 more then you end with 7 apples.
In short, addition is the accumulation of discrete quantities.
The best way to think about integration is that it is 
the accumulation of continuous quantities.

Recall that to compute $\int_{x=a}^bf(x)\,dx$ you can do this.
\begin{equation*}
  \lim_{n\to\infty}\;\sum_{0\leq i < n} f(\hat{x})(x_{i+1}-x_i)
\end{equation*}
where you have divided the interval from $a$ to~$b$ into $n$-many
subintervals, from $x_i$ to~$x_{i+1}$, and where $\hat{x}$ is the number
halfway between  $x_i$ and~$x_{i+1}$. 
\begin{example}
For $\int_{x=0}^1f(x)\,dx$ and $n=5$ intervals we have this.
% $x_0=0$, $x_1=0.2$, $x_2=0.4$, $x_3=0.6$, $x_4=0.8$, and $x_5=1.0$ 
% and this is the sum.
\begin{multline*}
  f(0.1)\cdot (0.2-0.0)
  +f(0.3)\cdot (0.4-0.2)
  +f(0.5)\cdot (0.6-0.4)  \\
  +f(0.7)\cdot (0.8-0.6)
  +f(0.9)\cdot (1.0-0.8)
\end{multline*}
\end{example}
\end{frame}


\begin{frame}
  \frametitle{Riemann sums}
Interpret this definition
\begin{equation*}
  \lim_{n\to\infty}\;\biggl[\,\sum_{i=1}^n\,f(\hat{x})\cdot \Delta x\,\biggr]
\end{equation*}
as ``take $n$ to be very large so that each interval is very thin, 
that is, $\Delta x$ is very small,
and then accumulate the quantities 
$f(\hat{x})\cdot \Delta x$.''
We walk the line segment stretching from $a$ to~$b$ with a clipboard,
totaling up the quantities  $f(\hat{x})\Delta x$.
In places where $f(\hat{x})$ is big then the quantity
contributes relatively more to the total, while in places where it is small
the quantity contributes relatively little.
Therefore we call $f$ the \alert{density function}.

We idealize that these very small $\Delta x$'s are infinitesimal 
and consequently alter $\Delta x$ to $dx$, alter $f(\hat{x})$ to $f(x)$,
and alter the summation sign to an integration sign.
\begin{equation*}
  \int_{x=a}^b f(x)\,dx
\end{equation*}

\pause
In doing the Calculus~I and~II integration of $\map{f}{\R}{\R}$ we
can always call the input real numbers $x$'s and show $f(x)$ on the
$y$-axis, so there is little loss in interpreting the integral as the 
area under the curve.
But in this class we will generalize 
$\map{f}{\R}{\R}$
to both $\map{f}{\R^n}{\R}$
and $\map{f}{\R}{\R^m}$, and we won't always be able to draw the graph.
The right way to think about these integrals is that we are
accumulating a density function pointwise on the domain.
\pause
(Although if thinking of it as the area under a curve is helpful,
we will feel free to do that.)
\end{frame}

\begin{frame}
  \frametitle{What is this sorcery?}

A person can reasonably object to viewing $\int_{x=0}^1 x^2\,dx$ as a sum
of infinitely many boxes, each of area zero.
How can we make sense of the answer $1/3$?

Said in a more insightful way, 
consider the five interval Reimann sum from earlier.
\begin{equation*}
  \sum_{0\leq i < 5} f(\hat{x})(x_{i+1}-x_i)
  =
  f(0.1)\cdot (0.2-0.0)
  +f(0.3)\cdot (0.4-0.2)
  +\cdots 
  +f(0.9)\cdot (1.0-0.8)
\end{equation*}
In taking the limit as the number of intervals goes to infinity, here
is a question.
We draw some pictures that convince ourselves 
that making the Reimann box narrower will make
$f(\hat{x})\cdot\Delta x$ a more accurate approximation of the
area under the curve and over that subinterval.
Even if that is true, suppose that in taking ten times as many boxes,
we halve the error of each box?
That's no good since total error will be five times as large.
\end{frame}

\begin{frame}
Our approach to handling paradoxes like this $\infty\cdot 0$ one
comes under the heading L'H\^opital's Rule.
The classic case is that both $f(x)=x$ and $g(x)=x^2$ go to zero as 
$x\to 0$, and now consider this comparison of the two.
\begin{equation*}
  \lim_{x\to 0}\frac{x}{x^2}=\lim_{x\to 0}\frac{1}{x}=\infty
\end{equation*}
Think of the limit of the fraction as a race.
The numerator going to zero that tends to take the entire
fraction to zero.
The denominator going to zero tends to take the entire
fraction to infinity.
So why does the denominator win?
It is the rates.
\begin{center}
  \begin{tabular}{r|*{3}{l}}
   \multicolumn{1}{r}{$x$}    
      &$0.1$  &$0.01$   &$0.001$          \\
   \hline 
   $f(x)$ &$0.1$  &$0.01$   &$0.001$         \\
   $g(x)$ &$0.01$ &$0.0001$ &$0.000001$      \\
   $f/g\,(x)$  &$10$   &$100$    &$1000$       
  \end{tabular}
\end{center}
% sage: n(0.1/0.01)
% 10.0000000000000
% sage: n(0.01/0.0001)
% 100.000000000000
% sage: n(0.001/0.000001)
% 1000.00000000000
Both $f$ and~$g$ go to zero, but $g$ goes there faster.

And that is the resolution for the paradox.
In this limit,
\begin{equation*}
  \lim_{n\to\infty}\; \sum_{0\leq i<n} f(\hat{x})\Delta x
  =\lim_{n\to\infty}\, f(\hat{x}_0)(x_1-x_0)
                  +\cdots f(\hat{x}_n)(x_{n+1}-x_n)
\end{equation*}
the error per term falls at a faster rate than the rate at which the number of
terms grows. 

The same will hold for the integrals in this class.
As with Calc~I and~II we shall omit the 
the arguments needed to precisely establish that we can take
the integrals to be the accumulation of continuous quantities,
in favor of getting to material that we need to cover.
\end{frame}


\begin{frame}
  \frametitle{Volume under a surface}

Consider $\map{f}{\R^2}{\R}$ given by $f(x,y)=4-2x^2-y^2$.
We want to find the volume under the surface and above the rectangle
$S=\set{(x,y)\suchthat 0\leq x\leq 1, 0\leq y\leq 1}$ (it is actually a square).
We divide~$S$ into lots of rectangular subregions, erect a box over each,
and take the sum as an approximation of the desired volume
\begin{equation*}
  V\approx \sum_{0\leq i< m} \;\sum_{0\leq j < n}\; f(\hat{x}_{i,j},\hat{y}_{i,j})\cdot \Delta A
\end{equation*}
where $(\hat{x}_{i,j},\hat{y}_{i,j})$ is the middle of the rectangle, and
$\Delta A$ is its area.
\begin{definition}
  The volume of the solid above a rectangle and below $z=f(x,y)$ is the
value of this limit, if it exists.
\begin{equation*}
  V=\lim_{m,n\to\infty}\;\biggl[\sum_{0\leq i< m} \;\sum_{0\leq j < n}\; f(\hat{x}_{i,j},\hat{y}_{i,j})\cdot \Delta A\biggr]
\end{equation*}
\end{definition}
\end{frame}


\begin{frame}
  \frametitle{Iterated integrals}

Consider again the solid under
$f(x,y)=4-2x^2-y^2$
and above
$S=\set{(x,y)\suchthat 0\leq x\leq 1, 0\leq y\leq 1}$.
Fix some~$x$ with $0\leq x\leq 1$.
We can find the area of the two-dimensional piece of the solid
$S_x=\set{(x,y)\suchthat 0\leq y\leq 1}$
with a \alert{partial integral}.
Treat $x$ as a constant in this computation.
\begin{equation*}
  A_x=
  \int_{y=0}^1 4-2x^2-y^2 dy
  =\bigl[ 4y-2x^2y-(1/3)y^3\bigr]_{y=0}^1
  =\bigl(4-2x^2+(1/3)\bigr)-\bigl(0-0-0\bigr)=(13/3)-2x^2
\end{equation*}
At different values of $x$, this piece has different areas.
\begin{center}
  \begin{tabular}{r|*{3}{c}}
    $x$   &$0$    &$0.5$  &$1$ \\
    \cline{2-4}
    $A_x$ &$13/3$ &$23/6$ &$7/3$ \\
  \end{tabular}
\end{center}
% sage: def f(x):
% ....:     return(n(13/3-2*x^2))
% ....: 
% sage: f(0)
% 4.33333333333333
% sage: f(0.5)
% 3.83333333333333
% sage: f(1)
% 2.33333333333333
\pause
We can draw this as a slice of the region.
Note that it is shown with infinitesimal volume, because it is $dx$~thick.
Now, compute the volume of the entire solid as the \alert{iterated integral}.
\begin{equation*}
  V=\int_{x=0}^1\;\Biggl[\int_{y=0}^1 4-2x^2-y^2\,dy\Biggr]\;dx
  =\int_{x=0}^1 \frac{13}{3}-2x^2 dx
  =\biggl[\frac{13}{3}x-\frac{2}{3}x^3\biggr]_{x=0}^1
  =\frac{11}{3}
\end{equation*}
We usually just write $\int_{x=0}^1\int_{y=0}^1\ldots dy\,dx$, without
the square brackets.
In general, we denote the problem of integrating $f$
over a region in the plane~$R$ as
\alert{$\iint f\,dA$}.
\end{frame}


\begin{frame}
\begin{exercise}
Find the volume of the solid under $z=f(x,y)=4+9x^2y^2$ and over
this rectangle.
\begin{equation*}
  R=\set{(x,y)\suchthat -1\leq x\leq 1,\,0\leq y\leq 2 }
\end{equation*}

\begin{itemize}
\pause\item
First sketch the region $R$ in the plane $\R^2$, and then sketch it in $\R^3$.
\pause\item
To the 3D sketch add a rough sketch of $f(x,y)$.
\pause\item
We will take slices $dx$~thick.
Draw the such slice at location~$x$ (for some $x$ with $-1\leq x\leq 1$).
It is rectangular on three sides, and the top is a curve.
On the bottom it is the line segment from $(x,0)$ to $(x,2)$.
Add that slice to the 3D picture.
\pause\item
Set up the iterated integral.
\begin{equation*}
  \int_{x=}^{x=}\;\int_{y=}^{y=}\; 4+9x^2y^2\,dy\;dx
\end{equation*}
\pause\item
Do the calculation.
You will get $[8x+8x^3]_{x=-1}^1=32$. 
\end{itemize}
\end{exercise}
\end{frame}

\begin{frame}
\begin{exercise}
Again find the volume of the solid under $z=f(x,y)=4+9x^2y^2$ and over
this rectangle.
\begin{equation*}
  R=\set{(x,y)\suchthat -1\leq x\leq 1,\,0\leq y\leq 2 }
\end{equation*}
This time do the slices~$dy$.    
\end{exercise}
\end{frame}


\begin{frame}
  \frametitle{Volumes over non-rectangular regions}
\begin{exercise}
Find the volume of the solid in the first octant below the plane 
$f(x,y)=1-x-y$.

\begin{itemize}
\item Draw the solid by finding the three corners where the plane intercepts
the axes.

\pause
The three corners are $(0,0,1)$, $(0,1,0)$, and $(1,0,0)$.

\pause\item
Sketch the base region.

\pause It is the $xy$-plane triangle with corners $(1,0)$ and $(0,1)$.
The line boundary is $y=1-x$.

\pause\item
Draw a $dx$-thick slice.

\pause\item Set up the double integral.
\pause 
\begin{equation*}
  \int_{x=0}^1\int_{y=0}^{1-x}1-x-y\,dy\,dx
\end{equation*}

\pause\item
Evaluate the integral.
\pause
\begin{align*}
   \int_{x=0}^1[(1-x)\cdot y -y^2/2]_{y=0}^{1-x}\;dx
  &
   =\frac{1}{2}\int_{x=0}^1 (1-x)^2\,dx  \\
  &
   =\frac{1}{2}[x-x^2+x^3/3]_{x=0}^1=\frac{1}{6} 
\end{align*}
\end{itemize}
\end{exercise}
\end{frame}

\begin{frame}
\begin{exercise}
Take $z=x^3+4y$.
Set up the double integral to 
find the volume of the solid under the surface and above the 
plane region between the line $y=2x$ and the parabola $y=x^2$.

\begin{itemize}
\item Take the slices $dx$.

\pause
\begin{equation*}
  \int_{x=0}^2\int_{y=x^2}^{y=2x} x^3+4y \,dy\,dx
\end{equation*}
\pause
\item Take the slices $dy$.

\pause
\begin{equation*}
  \int_{y=0}^2\int_{x=y/2}^{\sqrt{y}} x^3+4y \,dx\,dy
\end{equation*}
\end{itemize}
Both give $32/3$.
\end{exercise}
\end{frame}

\begin{frame}
\begin{exercise}
Find the volume of the solid below $f(x,y)=2x^2y$ and above the
plane region enclosed by $y=16-x^2$ and $y=3x^2$.
Note that the limits are $x=-2$ and $x=2$. 
Do the slices $dx$.

\textit{Hint:} You should end with this.
\begin{equation*}
  \int_{x=-2}^2 -8x^6-32x^4+256x^2\,dx
\end{equation*}
\end{exercise}

\pause
\end{frame}






\section{Double integrals in polars}

\begin{frame}
  \frametitle{Theory}
To find the volume under a surface and above a plane region~$R$, we've divided 
$R$ into rectangles that are $\Delta x$ by $\Delta y$ and so have
area $\Delta A=\Delta x\cdot\Delta y$.
Then over those rectangles we erected columns of height $f(x^*,y^*)$ 
and the integral was the limit of the sum of the products
$f(x^*\!,y^*) \Delta x\Delta y$.

But sometimes a plane region~$R$ is best described in polar coordinates.
Something happens in this case that requires explanation.

As before, we divide $R$ into subregions that are $\Delta r$ by $\Delta \theta$.
Over each one we will erect a column and proceed as before.
But these subregions are not rectangular,
each is part of an annulus.
They do not have area given by $\Delta r\cdot\Delta \theta$.
For instance, compare a $\Delta r$ thick subregion 
subtended by $\Delta \theta$
at radius~$1$ with the same at radius~$2$.
The second has twice the area.

\pause
To get the formula,
start with a circle and make a wedge subtended by an angle of $\theta$ radians.
The entire circle's area is $\pi r^2$ and there are $2\pi$ radians in a
circle, so the formula for the area of the wedge is $(1/2)\theta r^2$.

Next fix a point $(r^*\!,\theta^*)$ in the middle of one of the subregions
of we want.
Then the subregion runs from radius $r^*-(\Delta r/2)$ to 
radius $r^*+(\Delta r/2)$, and it covers the angle~$\Delta \theta$.
Its area is this.
\begin{align*}
  \Delta\theta \bigl[(1/2)(r+\Delta r/2)^2-(1/2)(r-\Delta r/2)^2\bigr]
  &=\Delta\theta/2 \bigl[(r^2+2r\Delta r/2+\Delta r^2)
                        -(r^2-2r\Delta r/2+\Delta r^2)\bigr]   \\
  &=\Delta\theta/2 \bigl[2r\Delta r\bigr]=r\Delta r\Delta\theta
\end{align*}
\end{frame}

\begin{frame}
  \frametitle{Result}
\begin{theorem}
Where $f$ is a continuous function defined on every point
in a region described using polar coordinates
\begin{equation*}
  R=\set{(r,\theta)\suchthat \alpha\leq\theta\let\beta,\,
                             r_1(\theta)\leq r\leq r_2(\theta)}
\end{equation*}
($\beta-\alpha$ is between $0$ and $2\pi$), then the volume
below $f$ and above~$R$ is this.
\begin{equation*}
  \iint_R f\,dA
  =\int_{\theta=\alpha}^{\beta}
      \int_{r=r_1(\theta)}^{r_2(\theta)}\;f(r\cos\theta,r\sin\theta)\,r\,dr\,d\theta
\end{equation*}
\end{theorem}

\begin{example}
  Find the volume of the half sphere $f(x,y)=9-x^2-y^2$ above the 
$xy$~plane.

\pause
The integral is this.
\begin{align*}
  \int_{\theta=0}^{2\pi}\int_{r=0}^3\;(9-r^2)\,r\,dr\,d\theta
  &=\int_{\theta=0}^{2\pi}\int_{r=0}^3\;9r-r^3\,dr\,d\theta  \\
  &=\int_{\theta=0}^{2\pi}\biggl[\frac{9}{2}r^2-\frac{1}{4}r^4\biggr]_{r=0}^3\,d\theta  \\
  &=\int_{\theta=0}^{2\pi}\frac{81}{4}\,d\theta=\frac{81\pi}{2}  
\end{align*}
\end{example}
\end{frame}


\begin{frame}
\begin{example}
  Find the volume below $f(x,y)=2x+3y$ and over the region 
between the circles of radius~$1$ and~$2$ in the first
quadrant.

\pause
\begin{align*}
  \iint_R2x+3y\,dA
  &=\int_{\theta=0}^{\pi/2}\;\int_{r=1}^2(2r\cos\theta+3r\sin\theta)\,r\,dr\,d\theta  \\
  &=\int_{\theta=0}^{\pi/2}\;\int_{r=1}^2(2r^2\cos\theta+3r^2\sin\theta)\,dr\,d\theta  \\
  &=\int_{\theta=0}^{\pi/2}\;\biggl[\frac{2}{3}r^3\cos\theta+r^3\sin\theta\biggr]_{r=1}^2\,d\theta  \\
  &=\int_{\theta=0}^{\pi/2}\frac{14}{3}\cos\theta+7\sin\theta\,d\theta  \\
  &=\biggl[\frac{14}{3}\sin\theta-7\cos\theta\biggr]_{\theta=0}^{\pi/2}\,d\theta =35/3 
\end{align*}
\end{example}
\end{frame}


\begin{frame}
Find an area by making the height $f(x,y)=1$.

\begin{example}
Find the area  enclosed by the cartiod $r=1-\cos\theta$.

\pause
\begin{align*}
  \iint 1\,dA 
  =\int_{\theta=0}^{2\pi}\int_{r=0}^{1-\cos\theta} 1\,r\,dr\,d\theta
  &=\int_{\theta=0}^{2\pi}\biggl[\frac{1}{2}r^2\biggr]_{r=0}^{1-\cos\theta}\,d\theta \\
  &=\int_{\theta=0}^{2\pi}\frac{1}{2}(1-\cos\theta)^2\,d\theta \\
  &=\frac{1}{2}\int_{\theta=0}^{2\pi}(1-2\cos\theta+\cos^2\theta)\,d\theta 
\end{align*}
Look up any antiderivatives that you don't know.
The answer is this.
\begin{equation*}
  \biggl[\theta-2\sin\theta+(\frac{\theta}{2}+\frac{\sin2\theta}{4})\biggr]_{\theta=0}^{2\pi}
  =\frac{3}{2}\pi
\end{equation*}
\end{example}
\end{frame}



% ========================
\section{Applications of double integrals}
\begin{frame}
  \frametitle{Mass of a lamina}
  Imagine that you have a thin sheet, a \alert{lamina}.
If we have electic charge distributed over it according to some 
function $\rho(x,y)$ then we find the total charge in the usual
Calculus way.
\begin{equation*}
  m\approx\sum_{1\leq i<k}\sum_{1\leq j<n} \rho(x_i^*,y_j^*) \Delta A
  \qquad
  m=\iint_L\rho(x,y)\,dA
\end{equation*}

\begin{example}
  Let $L$ be a think sheet in the plane bordered by the two axes and 
the line $y=-x+4$.
Take $\rho(x,y)=3x+2y^2$.

\pause
\begin{align*}
  \int_{x=0}^{4}\int_{y=0}^{-x+4}3x+2y^2\;dy\,dx
  &=
  \int_{x=0}^{4}\biggl[3xy+2y^3/3\biggr]_{y=0}^{-x+4}\;dx  \\
  &=
  \int_{x=0}^{4}3x(-x+4)+2(-x+4)^3/3\;dx  
\end{align*}
\end{example}
\end{frame}


\begin{frame}
\begin{example}
Find the mass of the triangle bounded by the axes and the line $x+y=a$ that
is made of material with a uniform density $\rho(x,y)=1$.    

\pause
\begin{align*}
  \int_{x=0}^{a}\int_{y=0}^{-x+a}\;1\;dy\,dx
  &=
  \int_{x=0}^{a}\biggl[y\biggr]_{y=0}^{-x+a}\,dx  \\
  &=
  \int_{x=0}^{a}-x+a\,dx  \\
  &=
  \biggl[-x^2/2+ax\biggr]_{x=0}^{a}=a/2
\end{align*}
Of course, that is half the square.
\end{example}
\end{frame}




\begin{frame}
  \frametitle{Surface area}
\begin{theorem}
Where $z=f(x,y)$ the area of the surface above the planar region $R$
is this.
\begin{equation*}
  \iint_R\sqrt{\frac{\partial f}{\partial x}^2+\frac{\partial f}{\partial y}^2+1}\;dA
\end{equation*}

\end{theorem}

\begin{example}
  The paraboloid $z=1-x^2-y^2$ intersects the $xy$~plane in the unit circle.
Find its surface area above the plane.

\pause
We have this.
\begin{equation*}
  S=\iint_R\sqrt{(-2x)^2+(-2y)^2+1}\;dA
  =\iint_R\sqrt{4(x^2+y^2)+1}\;dA
\end{equation*}
One way to solve it is to convert to polars.
\begin{align*}
  \int_{\theta=0}^{2\pi}\int_{r=0}^{1}\sqrt{4r^2+1}\cdot r\,dr\,d\theta
  &=
  \int_{\theta=0}^{2\pi}(1/12)\biggl[(4r^2+1)^{3/2}\biggr]_{r=0}^{1}\,d\theta \\ 
  &= \frac{\pi}{6}(5\sqrt{5}-1)
\end{align*}
\end{example}
\end{frame}



% ==================
\section{Integrals in 3D}
\begin{frame}
  \frametitle{Triple integrals}

\begin{example}
Integrate $f(x,y,z)=4xyz$ over the box $R$ defined by
$0\leq x \leq 1$, 
$0\leq y\leq 2$,
and $0\leq z\leq 3$.

\pause
That is, associated with each point $(x,y,z)$ is a density
$w=4xyz$, and we want the total mass inside the box.
\begin{align*}
  \int_{x=0}^{1}\int_{y=0}^{2}\int_{z=0}^{3}\;4xyz\;dz\,dy\,dx
  &=
  \int_{x=0}^{1}\int_{y=0}^{2}\;4xy\biggl[z^2/2\biggr]_0^3\;dy\,dx  \\
  &=
  \int_{x=0}^{1}\int_{y=0}^{2}\;18xy\;dy\,dx  \\
  &=
  \int_{x=0}^{1}\;18x\biggl[y^2/2\biggr]_0^2\,dx  
  =\biggl[36x^2\biggr]_0^1=18
\end{align*}
\end{example}
The integrand function could describe the density of mass, 
or of electric charge, or of volume.
\end{frame}


\begin{frame}
\begin{example}
Find the volume of the prism in the first octant bounded by 
the planes $y=4-2x$ and $z=6$.

\pause
This is the setup if we make slices $dz$.
\begin{equation*}
  \iiint_R\;1\,dV
  =
  \int_{z=0}^{6}\int_{x=0}^{2}\int_{y=0}^{4-2x}\;1\;dy\,dx\,dz
\end{equation*}
Here is the computation.
\begin{align*}
  \int_{z=0}^{6}\int_{x=0}^{2}\;\biggl[y\biggr]_{y=0}^{4-2x}\;dx\,dz
  &=
  \int_{z=0}^{6}\int_{x=0}^{2}\; 4-2x\;dx\,dz  \\
  &=
  \int_{z=0}^{6}\; \biggl[4x-x^2\biggr]_{x=0}^{2}\;dz  \\
  &=
  \int_{z=0}^{6} 4\,dz=24
\end{align*}
\end{example}
\end{frame}


\begin{frame}
\begin{example}
Where the density function is $f(x,y,z)=z$, find the integral over the region 
in the first octant bounded by $z=1-x^2$ and $y=x$.

\pause    
This is the setup if we make slices $dx$.
\begin{equation*}
  \iiint_R\;z\,dV
  =
  \int_{x=0}^{1}\int_{y=0}^{x}\int_{z=0}^{1-x^2}\;z\;dz\,dy\,dx
\end{equation*}
The computation is straightforward.
\begin{align*}
  \int_{x=0}^{1}\int_{y=0}^{x}\;\biggl[\frac{z^2}{2}\biggr]_{z=0}^{1-x^2}\;dy\,dx
  &=
  \frac{1}{2}\int_{x=0}^{1}\int_{y=0}^{x}\;(1-x^2)^2\;dy\,dx  \\
  &=
  \frac{1}{2}\int_{x=0}^{1}\biggl[(1-x^2)^2y\biggr]_{y=0}^{x}\;dx  \\
  &=
  \frac{1}{2}\int_{x=0}^{1}(1-x^2)^2x\,dx=\frac{1}{12}
\end{align*}
\end{example}
\end{frame}


\begin{frame}
  \frametitle{Triple integrals in cylindrical coordinates}

Recall that cylindrical coordinates have these conversions.
\begin{align*}
  &x=r\cos\theta\quad y=r\sin\theta\quad z=z  \\
  &r^2=x^2+y^2\quad \theta=\arctan(y/x)\quad z=z
\end{align*}

\begin{theorem}
\begin{equation*}
  \iiint_R f(x,y,z)\,dV
  =\int_{\theta=a}^{b}\int_{r=h_1(\theta)}^{h_2(\theta)}\int_{z=g_1(r\cos\theta,r\sin\theta)}^{g_2(r\cos\theta,r\sin\theta)}\;f(r\cos\theta,r\sin\theta,z)\cdot r\,dz\,dr\,d\theta     
\end{equation*}
\end{theorem}
\end{frame}

\begin{frame}
\begin{example}
Find the volume of the solid~$R$ between the cone $z=\sqrt{x^2+y^2}$ and the
paraboloid $z=12-x^2-y^2$.

\pause
We can describe the solid in cylindricals as lying between 
$z=r$ and $z=12-r^2$, and note that the two intersect at $r=3$.
Observe that the theorem tells us how to do the slicing.
\begin{equation*}
 \iiint_R 1\,dV
 =
 \int_{\theta=0}^{2\pi}\int_{r=0}^{3}\int_{z=r}^{12-r^2} \;1\cdot r\,dz\,dr\,d\theta
\end{equation*}
The computation is routine.
\begin{align*}
 \int_{\theta=0}^{2\pi}\int_{r=0}^{3}\;(12-r^2-r)r\;dr\,d\theta 
 &=
 \int_{\theta=0}^{2\pi}\int_{r=0}^{3}\;12r-r^3-r^2\;dr\,d\theta  \\ 
 &=
 \int_{\theta=0}^{2\pi}\frac{99}{4}\,d\theta=\frac{99\pi}{2} 
\end{align*}

\end{example}
\end{frame}




\begin{frame}
  \frametitle{Triple integrals in spherical coordinates}

Recall that spherical coordinates have these conversions.
\begin{align*}
  &x=\rho\sin\phi\cos\theta\quad y=\rho\sin\phi\sin\theta\quad z=\rho\cos\phi  \\
  &\rho^2=x^2+y^2+z^2
\end{align*}
where $\rho$ is the distance of $(x,y,z)$ from the origin,
$\theta$ is the angle that the projection makes with the $x$-axis,
and $\phi$ is the angle of declination from the vertical.

\begin{theorem}
\begin{equation*}
  \iiint_R f(x,y,z)\,dV
  =\int_{\phi=c}^{d}\int_{\theta=\alpha}^{\beta}\int_{\rho=a}^{b}\;f(\rho\sin\phi\cos\theta,\rho\sin\phi\sin\theta,\rho\cos\phi)\cdot \rho^2\sin\phi\cdot\,d\rho\,d\theta\,d\phi     
\end{equation*}
\end{theorem}
\end{frame}


\begin{frame}
\begin{example}
Find the volume of the first octant region below the sphere
$x^2+y^2+z^2=1$.

\pause
\begin{equation*}
 \iiint_R 1\,dV
 =
  \int_{\theta=0}^{\pi/2}\int_{\phi=0}^{\pi/2}\int_{\rho=0}^{1}\;1\cdot\rho^2\sin\phi\;d\rho\,d\phi\,d\theta
\end{equation*}
The computation will give the volume of an eigth of a sphere, 
$\frac{1}{8}(\frac{4}{3}\pi\cdot 1^3)$.
\begin{align*}
  \int_{\theta=0}^{\pi/2}\int_{\phi=0}^{\pi/2}\biggl[\frac{\rho^3}{3}\biggr]_{\rho=0}^{1}\cdot\sin\phi\;d\phi\,d\theta
  &=
  \int_{\theta=0}^{\pi/2}\int_{\phi=0}^{\pi/2}\;\frac{1}{3}\sin\phi\;d\phi\,d\theta  \\
  &=
  \frac{1}{3}\int_{\theta=0}^{\pi/2}\biggl[-\cos\phi\biggr]_{\phi=0}^{\pi/2}\,d\theta  \\
  &=
  \frac{1}{3}\int_{\theta=0}^{\pi/2}1\,d\theta=\frac{1}{3}\cdot\frac{\pi}{2}
\end{align*}
\end{example}
\end{frame}



\section{Change of variables}
% =================================
\begin{frame}
  \frametitle{Who put the $r$ in the $r\,dr\,d\theta$?}
% Where does the `$r$' come from in $r\,dr\,d\theta$?
% What about the $\rho^2\sin\phi$?

Consider this Calculus~I substitution.
\begin{equation*}
  \int_{x=0}^12\sqrt{2x+1}\,dx
  \qquad
  \int_{u=1}^3 \sqrt{u}\,du
\end{equation*}
It comes from the change of variables $u=T(x)=2x+1$, along with the
derivative relation $du=2\,dx$.
If you compare the graph on the $xy$ axes to the graph on the $uy$ axes
then you see that a $\Delta x$-wide box doubles in width in becoming a 
$\Delta u$-wide box.

\pause
We have seen the transformation.
\begin{equation*}
  \begin{array}{rr@{\hspace{0.1111em}}l}
  T: &x(r,\theta) &= r\cos\theta  \\
     &y(r,\theta) &= r\sin\theta
  \end{array}
\end{equation*}
We know that a rectangle in the $r\theta$~plane 
becomes a part of an annulus in the $xy$~plane.
And the $r$ in $r\,dr\,d\theta$ is the relationship between
the size of the boxes.
\begin{center}
  \includegraphics[height=1in]{asy/integrals000.pdf}
  \hspace*{0.5in}
  \includegraphics[height=1in]{asy/integrals001.pdf}
\end{center}

\end{frame}


\begin{frame}
\begin{definition}
  Where the transformation $T$ is defined by 
$x=g(u,v)$ and $y=h(u,v)$, the \alert{Jacobean} is this
determinant.
\begin{equation*}
  \frac{\partial(x,y)}{\partial(u,v)}
  =\begin{vmatrix}
     \frac{\partial x}{\partial u}  &\frac{\partial x}{\partial v}  \\[1ex]
     \frac{\partial y}{\partial u}  &\frac{\partial y}{\partial v}  
   \end{vmatrix}
  =\frac{\partial x}{\partial u}\frac{\partial y}{\partial v}
    -\frac{\partial y}{\partial u}\frac{\partial x}{\partial v}
\end{equation*}
In the above transformation, the areas of the regions are related by
$\text{area}(T(R))=\text{Jacobean}(T)\cdot\text{area}(R)$.
\end{definition}

\begin{example}
  For the polar coordinate transformation $T$
described by $x=g(r,\theta)=r\cos\theta$ and 
$y=h(r,\theta)=r\sin\theta$ we have this.
\begin{equation*}
  J=\frac{\partial (x,y)}{\partial (r,\theta)}
  =\begin{vmatrix}
  \cos\theta  &-r\sin\theta \\
  \sin\theta  &r\cos\theta
   \end{vmatrix}
  =r
\end{equation*}
\end{example}
\end{frame}

\begin{frame}
\begin{example}
Use $x=u^2-v^2$ and $y=2uv$ to evaluate $\iint_Ry\,dA$ where
$R$ is the region bounded by $y^2=4+4x$ on the left, $y^2=4-4x$ on the
right, and on the bottom by the $x$~axis between $(-1,0)$ and $(1,0)$.

\pause
First,
\begin{equation*}
  \frac{\partial(x,y)}{\partial(u,v)}=
  \begin{vmatrix}
    \frac{\partial x}{\partial u} &\frac{\partial x}{\partial v}  \\
    \frac{\partial y}{\partial u} &\frac{\partial y}{\partial v}  \\
  \end{vmatrix}=
  \begin{vmatrix}
    2u &-2v \\
    2v &2u
  \end{vmatrix}
  =4u^2+4v^2
\end{equation*}
so we have this.
\begin{align*}
  \iint_Rf(x,y)\,dA
  =\iint_Sf(x(u,v),y(u,v)) \cdot \frac{\partial(x,y)}{\partial(u,v)} \;du\,dv
  &=\int_{v=0}^{1}\int_{u=0}^{1}\;2uv\cdot 4(u^2+v^2)\;du\,dv  \\
  &=8\int_{v=0}^{1}\int_{u=0}^{1}\; u^3v+uv^3\;du\,dv=2  \\
\end{align*}
\end{example}
\end{frame}


\begin{frame}
  \frametitle{Triple integrals}

\begin{lemma}
Suppose that the transformation $T$ is described by 
$x=g(u,v,w)$, and     
$y=h(u,v,w)$, and     
$z=k(u,v,w)$.
Suppose further that $T$ maps a region $S$ in $uvw$~space onto a region 
$R$ in $xyz$~space. 
Then this determinant is the \alert{Jacobean} of $T$
\begin{equation*}
  \frac{\partial (x,y,z)}{\partial(u,v,w)}=
  \begin{vmatrix}
    \frac{\partial x}{\partial u} 
       &\frac{\partial x}{\partial v} 
       &\frac{\partial x}{\partial w}    \\[1ex] 
    \frac{\partial y}{\partial u} 
       &\frac{\partial y}{\partial v} 
       &\frac{\partial y}{\partial w}    \\[1ex] 
    \frac{\partial z}{\partial u} 
       &\frac{\partial z}{\partial v} 
       &\frac{\partial z}{\partial w}   
  \end{vmatrix}
\end{equation*}
and we have this.
\begin{equation*}
  \iiint_Rf(x,y,z)\,dV
  =\iiint_Sf(\,g(u,v,w),h(u,v,w),k(u,v,w)\,)\cdot  
       \frac{\partial (x,y,z)}{\partial(u,v,w)}=
       \;du\,dv\,dw
\end{equation*}
\end{lemma}
\end{frame}

\begin{frame}
For spherical coordinates
\begin{equation*}
  x=\rho\sin\phi\cos\theta
  \qquad
  y=\rho\sin\phi\sin\theta
  \qquad
  z=\rho\cos\phi
\end{equation*}
this is the Jacobean.
\begin{equation*}
   \frac{\partial (x,y,z)}{\partial(\rho,\phi,\theta)}=
   \begin{vmatrix}
     \sin\phi\cos\theta &\rho\cos\phi\cos\theta &-\rho\sin\phi\sin\theta \\
     \sin\phi\sin\theta &\rho\cos\phi\sin\theta &\rho\sin\phi\cos\theta \\
     \cos\phi           &-\rho\sin\phi          &0 \\
   \end{vmatrix}
   =\rho^2\sin\phi
\end{equation*}
\end{frame}


\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 







