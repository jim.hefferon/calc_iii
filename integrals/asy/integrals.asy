// integrals.asy
//  Calculus III material
// 2023-Apr-07 Jim Hefferon GPL

import settings; 
settings.outformat="pdf";
settings.render=0;

unitsize(1pt);

usepackage("amsmath");
// Set LaTeX defaults
// import settexpreamble;
// settexpreamble();
// // Asy defaults
// import jhnode;

string OUTPUT_FN = "integrals%03d";


import graph;


// ===== Change of variables
int picnum = 0;
picture pic;

size(pic, 4inches,0);

path rtheta_rectangle = (1,1)--(3,1)--(3,2)--(1,2)--cycle;
draw(pic,rtheta_rectangle);
label(pic,"$\Delta r$",point(rtheta_rectangle,0.5),S);
label(pic,"$\Delta \theta$",point(rtheta_rectangle,3.5),W);

xaxis(pic,"$r$",
      xmin=-0.25,xmax=4.25,
      // RightTicks(Step=10,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"$\theta$",
      ymin=-0.25,ymax=4.25,
      // LeftTicks(Step=10,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");


// ........................
int picnum = 1;
picture pic;

size(pic, 4inches,0);

path inside_arc = arc((0,0), 2, 30, 60);
path outside_arc = arc((0,0), 3, 30, 60);
path right_side = point(inside_arc,0)--point(outside_arc,0);
path left_side = point(inside_arc,1)--point(outside_arc,1);
draw(pic,inside_arc);
draw(pic,right_side);
draw(pic,outside_arc);
draw(pic,left_side);
label(pic,"$\Delta r$",point(inside_arc,0.5),SW);
label(pic,"$\Delta \theta$",point(left_side,0.5),NW);

pair region_center = (2.5*Cos(45),2.5*Sin(45));
dot(pic, region_center, Draw);
label(pic, "\small $(r,\theta)$", region_center, N);

label(pic, "$\text{Area}=r\Delta r\Delta\theta$", point(outside_arc,0.5), 2*NE);

// draw(pic, (0,0)--point(inside_arc,1), red);
// draw(pic, (0,0)--point(inside_arc,0), red);



xaxis(pic,"$x$",
      xmin=-0.25,xmax=4.25,
      // RightTicks(Step=10,OmitTick(0)),
      Arrows(TeXHead));
yaxis(pic,"$y$",
      ymin=-0.25,ymax=4.25,
      // LeftTicks(Step=10,OmitTick(0)),
      Arrows(TeXHead));

shipout(format(OUTPUT_FN,picnum),pic,format="pdf");

