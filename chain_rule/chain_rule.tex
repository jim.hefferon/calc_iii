\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}

% \usepackage[perpage,symbol*,para,multiple]{footmisc}
% Getting undefined commands in aux file
\makeatletter
\newcommand\FN@pp@footnote@aux[2]{\relax}
\makeatother
\usepackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 

% set the fonts
\usefonttheme{professionalfonts} % using non standard fonts for beamer
\usefonttheme{serif} % default family is serif
\usepackage{../../presentation,../../presentationfonts}
% \usepackage{jh}
% \newcommand{\colvec}[1]{%
%   \begin{pmatrix} #1 \end{pmatrix}}
\usepackage{../calciii}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Chain Rule}

\author{Jim Hef{}feron}
\institute{
  Mathematics and Statistics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{hefferon.net}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Chain Rule}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}




% \section{Tangent Plane}
% ------------------------------------------------------
\begin{frame}
  \frametitle{Recall what derivatives are}

Here is an application of the prior section's thinking about derivatives.

\begin{example}
Let $f(x)=x^2$.
We consider the function's behavior  near $x=3$.
We say that $f(3+h)\approx f(3)+6h$ is the linear approximation
to the function near the input~$3$.
Restated,
near the input~$3$ the function's output changes are
$6$ times as big as the input changes.

We can picture this by starting with 
a neighborhood around the p~$x_0=3$, the interval 
$\open{3-\Delta x}{3+\Delta x}$, where $\Delta x$ is quite small.
The function $f$ takes $x$'s in that interval and maps them 
to outputs in the interval $\open{9-6\cdot\Delta x}{9+6\cdot\Delta x}$.

\vspace*{1.25cm}
(This picture gives a general ideal, but does not cover cases such
as what happens if the poin of interest is a local max or min.)

\pause
Then for the composition of two functions the picture is this.

\vspace*{1.25cm}
This is the Chain Rule.
\begin{equation*}
  \frac{d\,\composed{g}{f}}{dx}(x_0)
  =\frac{dg}{dx}(f(x_0))\cdot \frac{df}{dx}(x_0)
\end{equation*}
\end{example}
\end{frame}


\begin{frame}
  \frametitle{Composition in 3D}

\begin{example}
Let $z=f(x,y)=3x^2+4y^3$.
Also let $x=x(t)=t^3+9$ and $y=y(t)=t^2-4$.
How does $z$ change as $t$ changes?

Fix some $t_0=5$ and consider going from $5$ to $5+\Delta t$.
Then we know how to compute the change in $x$ along with the 
change in $y$.
\begin{equation*}
  \frac{dx}{dt}=3t^2
  \text{\ so\ }\frac{dx}{dt}(5)=75 
  \qquad
  \frac{dy}{dt}=2t
  \text{\ so\ }\frac{dy}{dt}(5)=10
\end{equation*}
So in going from input $t_0=5$ to $5+\Delta t$, 
the $x$ output goes from $x_0=x(5)=134$ to $134+75\cdot \Delta t$.
Likewise, in going from $t_0=5$ to $5+\Delta t$, 
the $y$ output goes from $y_0=y(5)=21$ to $21+10\cdot \Delta t$.

Now we are done because we know how $z$ changes as $x$ and $y$ changes:
$\Delta z \approx \frac{\partial\, f}{\partial x}\cdot \Delta x
                  +\frac{\partial\, f}{\partial y}\cdot \Delta y$.
\end{example}

\begin{theorem}[Chain Rule]
Let $\map{f}{\R^2}{\R}$, and let $x=x(t)$ along with $y=y(t)$.
\begin{equation*}
  \frac{\partial f}{\partial t}
  =\frac{\partial f}{\partial x}\cdot \frac{dx}{dt}
   +\frac{\partial f}{\partial y}\cdot \frac{dy}{dt}
\end{equation*}
The generalization to functions $\map{f}{\R^n}{\R}$ is natural.
\begin{equation*}
  \frac{\partial f}{\partial t}
  =\frac{\partial f}{\partial x_1}\cdot \frac{\partial x_1}{\partial t}
   +\frac{\partial f}{\partial x_2}\cdot \frac{\partial x_2}{\partial t}
   +\cdots 
   +\frac{\partial f}{\partial x_n}\cdot \frac{\partial x_n}{\partial t}
\end{equation*}
\end{theorem}
\end{frame}


\begin{frame}
\begin{example}
Suppose that $z=\sin(2x)\cdot \cos(3y)$
where $x=s+t$ and $y=s-t$.
The theorem gives these.
\begin{align*}
  \frac{\partial z}{\partial s}
  &=\frac{\partial z}{\partial x}\cdot \frac{\partial x}{\partial s}
    +\frac{\partial z}{\partial y}\cdot \frac{\partial y}{\partial s}   \\
  \frac{\partial z}{\partial t}                                    
  &=\frac{\partial z}{\partial x}\cdot \frac{\partial x}{\partial t}
    +\frac{\partial z}{\partial y}\cdot \frac{\partial y}{\partial t}
\end{align*}
\pause
Plugging in for the first gives this.
\begin{align*}
  \frac{\partial z}{\partial s}
  &=2\cos(2x)\cdot\cos(3y)\cdot 1+(-3\sin(2x)\cdot\sin(3y)\cdot 1)     \\
  &=2\cos(2s+2t)\cdot\cos(3s-3t)-3\sin(2s+2t)\cdot\sin(3s-3t)
\end{align*}
Similarly
\begin{align*}
  \frac{\partial z}{\partial t}
  &=2\cos(2x)\cdot\cos(3y)\cdot 1+(-3\sin(2x)\cdot\sin(3y)\cdot -1)     \\
  &=2\cos(2s+2t)\cdot\cos(3s-3t)+3\sin(2s+2t)\cdot\sin(3s-3t)
\end{align*}
(note the change in sign in the final term).
\end{example}

\begin{exercise}
Let $z=x^2\cdot e^y$ and $x=2s-t$, $y=3t^2$.  Find $\partial z/\partial s$
and $\partial z/\partial t$  
\end{exercise}
\end{frame}


\begin{frame}
  \frametitle{Implicit differentiation}
Start with a function of two variables $f(x,y)$.
Setting it to zero  $f(x,y)=0$ implicitly defines $y$ as a function of~$x$.
\begin{equation*}
  \frac{dy}{dx}=-\frac{\partial f/\partial x}{\partial f/\partial y}
\end{equation*}
Note that the letters $x$ and $y$ have opposite places in the fractions.

\begin{example}
Let $f(x,y)=\sin(xy)+\pi\cdot y^2-x=0$.
Then $\partial f/\partial x=y\cos(xy)-1$ 
and $\partial f/\partial y=x\cos(xy)+2\pi y$.
Thus we have this.
\begin{equation*}
  \frac{dy}{dx}
  =-\frac{y\cos(xy)-1}{x\cos(xy)+2\pi y}
\end{equation*}

\end{example}
\end{frame}
\end{document}







