\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}

% \usepackage[perpage,symbol*,para,multiple]{footmisc}
% Getting undefined commands in aux file
\makeatletter
\newcommand\FN@pp@footnote@aux[2]{\relax}
\makeatother
\usepackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 

% set the fonts
\usefonttheme{professionalfonts} % using non standard fonts for beamer
\usefonttheme{serif} % default family is serif
\usepackage{../../presentation,../../presentationfonts}
% \usepackage{jh}
% \newcommand{\colvec}[1]{%
%   \begin{pmatrix} #1 \end{pmatrix}}
\usepackage{../calciii}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Linear approximation and the tangent plane}

\author{Jim Hef{}feron}
\institute{
  Mathematics and Statistics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{hefferon.net}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Tangent plane}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}




% \section{Tangent Plane}
% ------------------------------------------------------
\begin{frame}
  \frametitle{Recall what derivatives are}

In this course we extend ideas from the prior two courses to higher
dimensions.
In Calc~I we define the derivative of a function as its rate of change.

\begin{example}
Let $f(x)=x^2$.
We will consider the function's behavior  near $x=3$.
\begin{center}\small
  \begin{tabular}{ccc}
    \multicolumn{1}{c}{\tabulartext{When the input changes}}  
    &\multicolumn{1}{c}{\tabulartext{the output changes}}
    &\multicolumn{1}{c}{\tabulartext{the difference $f(3+h)-f(3)$} is}    \\
    \hline
    from $x=3$ to $x=2.9$  &from $y=9$ to $y=8.41$   &$-0.59$  \\
    from $x=3$ to $x=2.99$ &from $y=9$ to $y=8.9401$ &$-0.0599$  \\
    from $x=3$ to $x=3$    &--                  &$0$  \\
    from $x=3$ to $x=3.01$ &from $y=9$ to $y=9.0601$ &$0.0601$  \\
    from $x=3$ to $x=3.1$  &from $y=9$ to $y=9.61$   &$0.61$  \\
  \end{tabular}
\end{center}
\end{example}  
The point is that the right-hand column is essentially $6h$. 
That is, 
$f(3+h)-f(3)=6h+\varepsilon$ for some small error $\varepsilon$.
Further, as $h$ goes to zero the approximation gets better and better,
so $h\to 0$ implies $\varepsilon\to 0$.

\begin{center}
\fbox{\alert{In a neighborhood of $x=3$ the function's output changes at a rate of~$6$.}}
\end{center}

We say that $f(3+h)\approx f(3)+6h$ is the \alert{linear approximation}
to the function near $x=3$.

Of course, all this holds for a wide class of functions at a wide array of
points.
\end{frame}


\begin{frame}
  \frametitle{Development of the idea}
\begin{enumerate}
\item More traditional notation writes $\Delta x$ instead of $h$.
For small $\Delta x$'s, such as $\Delta x=0.001$, we have that
$\Delta y\approx 6\cdot \Delta x$.
The inventors of Calculus idealized to infinitesimals.
\begin{equation*}
  \frac{\Delta y}{\Delta x}\approx 6
  \quad\text{becomes}\quad
  \frac{dy}{dx}=6
\end{equation*}

\pause\item 
In our Calc~I 
we define the derivative as here.
\begin{equation*}
    \left.\frac{df}{dx}\right|_{x=3}=\lim_{h\to 0}\frac{f(3+h)-f(3)}{h}=6
\end{equation*}


\item
There are all kinds of rules to master, 
such as the derivatives of polynomials, exponentials, trigonometrics, etc.,
and the derivatives of the products of functions, or the composition of
functions.

\item
Thinking that near $x=3$ the function has a growth rate of $6$ 
is something that 
we can leverage this in all kinds of ways, such as that
if the derivative is zero then the function is at a local maximum or
minimum.

\pause\item
There is a geometric interpretation.
Near $(x,y)=(3,9)$ the function behaves like a 
line of slope~$6$.
This is \alert{the line tangent to the curve}
at that point.
The point-slope form is 
$y-f(3)=6\cdot (x-3)$.
\end{enumerate}


So rate of change is what the derivative is, and the tangent line is one way
that we can think of it.
\end{frame}


\begin{frame}
  \frametitle{Linear aproximation in higher dimensions}

For a function $\map{f}{\R^2}{\R}$ we know how to compute the rate of change
when only one variable changes; this is the partial differentation.
We now leverage those to extend to changing more than one variable
at a time.

Consider $f(x,y)=4-2x^2-y^2$ in a neighborhood of $(x,y)=(1,1,1)$. 
\begin{align*}
  \frac{\partial\,f}{\partial x} &= -4x  
      &\left.\frac{\partial\,f}{\partial x}\right|_{(1,1)} &= -4  \\
  \frac{\partial\,f}{\partial y} &= -2y  
      &\left.\frac{\partial\,f}{\partial y}\right|_{(1,1)} &= -2  
\end{align*}
Near $x=1$ if we freeze $y$ then the function changes at a rate of $-4$,
and near $y=1$ if we freeze $x$ then the function changes at a rate of $-2$.
Extending the Calc~I case we have this.
\begin{equation*}
  f(1+\Delta x,1+\Delta y)
  \approx 1+(-4)\Delta x+(-2)\Delta y
\end{equation*}
For instance, taking $\Delta x=0.03$ and $\Delta y=-0.02$ we have 
\begin{align*}
  &f(1+0.03,1-0.02)=4-2(1.03)^2-(0.98)^2=0.9178 \\
  &f(1,1)+(-4)(0.03)+(-2)(-0.02)=0.92
\end{equation*}

\pause
\begin{definition}
The \alert{linear approximation of $\map{f}{\R^2}{\R}$} 
at $(x_0,y_0)$ is
\begin{equation*}
  L(x,y)
  =f(x_0,y_0)
   +\left.\frac{\partial\,f}{\partial x}\right|_{(x_0,y_0)}\!\cdot(x-x_0)
   +\left.\frac{\partial\,f}{\partial y}\right|_{(x_0,y_0)}\!\cdot(y-y_0)
\end{equation*}
(assuming that the partials exist).
\end{definition}
\end{frame}


\begin{frame}
\begin{exercise}
Find the linear approximation of $f(x,y)=2x^3y-y^2$ 
where $(x_0,y_0)=(-1,5)$.    
\end{exercise}
\end{frame}


\begin{frame}
  \frametitle{Differentiability}
\begin{definition}
\alert{A function $\map{f}{\R^2}{\R}$ is differentiable at $(x_0,y_0)$}
if the errors in the linearization go to zero: 
\begin{equation*}
  f(x_0+\Delta x,y_0+\Delta y)-f(x_0,y_0)
  =
    \left.\frac{\partial\,f}{\partial x}\right|_{(x_0,y_0)}\!\cdot\Delta x
   +\left.\frac{\partial\,f}{\partial y}\right|_{(x_0,y_0)}\!\cdot\Delta y
   +\varepsilon_1\Delta x+\varepsilon_2\Delta y
\end{equation*}
satisfies that as $(\Delta x,\Delta y)\to(0,0)$
then $\varepsilon_1\to 0$ and $\varepsilon_2\to 0$.
\end{definition}

% \pause
% \begin{remark}
% The following function shows that continuity of the partials doesn't guarantee
% continuity of the function at $(x_0,y_y)$, so that is a separate condition
% for differentiability.
% \begin{equation*}
%   f(x,y)=
%   \begin{cases}
%     \frac{xy}{x^2+y^2}  &\case{if $(x,y)\neq(0,0)$}  \\
%     0                   &\case{otherwise}
%   \end{cases}
% \end{equation*}
% It is rational so the only question about continuity and 
% partial differentiability
% at the origin.
% Approaching $(0,0)$ along the line $y=mx$
% \begin{equation*}
%   \lim_{(x,y)\to(0,0)} \frac{mx^2}{x^2+m^2x^2}
%   =\lim_{(x,y)\to(0,0)} \frac{m}{1+m^2}
%   =\frac{m}{1+m^2}
% \end{equation*}
% varies with $m$, so the function is not continuous at $(0,0)$.
% However, the partials exist.
% \begin{equation*}
%   \left.\frac{\partial\,f}{\partial x}\right|_{(0,0)}
%   =\lim_{h\to 0}\frac{\frac{(0+h)\cdot 0}{(0+h)^2+0^2}
%                       -\frac{0\cdot 0}{0^2+0^2}}{h}
%   =0
% \end{equation*}
% \end{remark}
\end{frame}


\begin{frame}
  \frametitle{Total differential}

Where the function $\map{f}{\R^2}{\R}$ is differentiable at $(x_0,y_0)$, 
in the linear approximation
\begin{equation*}
  L(x_0+\Delta x,y_0+\Delta y)
  =f(x_0+\Delta x,y_0+\Delta y)
   +\left.\frac{\partial\,f}{\partial x}\right|_{(x_0,y_0)}\!\cdot\Delta x
   +\left.\frac{\partial\,f}{\partial y}\right|_{(x_0,y_0)}\!\cdot\Delta y
\end{equation*}
the error $L(x_0+\Delta x,y_0+\Delta y)-f(x_0+\Delta x,y_0+\Delta y)$ 
falls to zero.
\pause
As with Calc~I, 
we idealize to this.

\begin{definition}
The \alert{total differential of $\map{f}{\R^2}{\R}$} is this.
\begin{equation*}
  dz=\frac{\partial\,f}{\partial x}dx
      +\frac{\partial\, f}{\partial y}dy
\end{equation*}
\end{definition}

\begin{exercise}
  Find the total differential of $f(x,y)=3xy-x^2y^2$.
\end{exercise}
\end{frame}

\begin{frame}
  \frametitle{Tangent plane}
\begin{definition}
The \alert{plane tangent to \map{f}{\R^2}{\R}} at $(x_0,y_0)$ is
\begin{equation*}
  z-z_0 = \left.\frac{\partial\,f}{\partial x}\right|_{(x_0,y_0)}\!\cdot(x-x_0)
        + \left.\frac{\partial\,f}{\partial y}\right|_{(x_0,y_0)}\!\cdot(y-y_0)
\end{equation*}
where $z_0=f(x_0,y_0)$.
\end{definition}

\pause
\begin{example}
Find the plane tangent to $f(x,y)=5x+4y^2$ at $(x_0,y_0)=(2,1)$.
 
Compute these.
\begin{equation*}
  f(2,1)=14
  \quad
  \left.\frac{\partial\,f}{\partial x}\right|_{(2,1)}=5
  \quad
  \left.\frac{\partial\,f}{\partial y}\right|_{(2,1)}=8
\end{equation*}
to get
$z-14=5\cdot(x-2)+8(y-1)$,
which we can rewrite as $z=5x+8y-4$.
\end{example}

\begin{exercise}
Find the plane tangent to $g(x,y)=\cos(xy)$ at $(x,y)=(\pi,4)$.  
\end{exercise}
\end{frame}

\end{document}







