import graph;
size(4inches,0);

real f0(real x) {return ((1/2)*x^2)+3;}  // x^2-2y=-6
real f1(real x) {return ((1/2)*x^2);}    // x^2-2y=0
real f2(real x) {return ((1/2)*x^2-5);}  // x^2-2y=10

xaxis("$x$",LeftTicks);
yaxis("$y$",ymin=-5.5,ymax=4.5,RightTicks);

real LEFTX = -4;
real RIGHTX = 4;
draw(Label("\small $g(x,y)=-6$",Relative(1)),graph(f0,-sqrt(2),sqrt(2)),blue);  // make hgt y=4 
draw(graph(f1,-sqrt(8),sqrt(8)),blue);   // make hgt be y=4
draw(Label("\small $g(x,y)=10$",Relative(0)),graph(f2,-sqrt(18),sqrt(18)),blue); //


// draw circle
real x(real t) {return 3*cos(2pi*t);}
real y(real t) {return 3*sin(2pi*t);}
draw(graph(x,y,0,1),red);
label("\small $x^2+y^2=9$",(1.5,-1),red);

// draw("$y=4-x$",graph(f2,1,5),LeftSide,red,Arrow);
// dot((1,f2(1)),red);
