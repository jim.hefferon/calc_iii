\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}

% \usepackage[perpage,symbol*,para,multiple]{footmisc}
% Getting undefined commands in aux file
\makeatletter
\newcommand\FN@pp@footnote@aux[2]{\relax}
\makeatother
\usepackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 

% set the fonts
\usefonttheme{professionalfonts} % using non standard fonts for beamer
\usefonttheme{serif} % default family is serif
\usepackage{../../presentation,../../presentationfonts}
% \usepackage{jh}
% \newcommand{\colvec}[1]{%
%   \begin{pmatrix} #1 \end{pmatrix}}
\usepackage{../calciii}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Optima}

\author{Jim Hef{}feron}
\institute{
  Mathematics and Statistics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{hefferon.net}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Optima}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}




% ------------------------------------------------------
\begin{frame}
  \frametitle{Finding maximima and minima}

In Calculus~I and important application of the derivative is finding
maxima and minima.
The ideas generalize to 3D, but with some twists.

\begin{definition}
A function $\map{f}{\R^2}{\R}$ has a 
\alert{relative minimum} (or \alert{local minimum}) at $(a,b)$
if there is a neighborhood of $(a,b)$, a small circle, 
where for all points $(x,y)$ in that neighborhood,
$f(x,y)\geq f(a,b)$. 
Similarly, $f$ has a \alert{relative maximum} (or \alert{local maximum})
at $(a,b)$  
if $f(x,y)\leq f(a,b)$ for all $(x,y)$ in some small neighborhood of $(a,b)$.
\end{definition}
\end{frame}


\begin{frame}
  \frametitle{First Derivative Test}

\begin{lemma}
If $(a,b)$ is a relative minimum of $f$ then both of these hold.
\begin{equation*}
  \frac{\partial\,f}{\partial x}(a,b)=0
  \qquad
  \frac{\partial\,f}{\partial y}(a,b)=0
\end{equation*}
\end{lemma}

\begin{definition}
For the function $\map{f}{\R^2}{\R}$,
a point $(a,b)$ is a \alert{critical point} (or \alert{stationary point})
if at $(a,b)$ the two partials are 0, or if either partial doesn't exist.  
\end{definition}

Critical points are candidates for local optima.

\pause
\begin{example}
  Find the critical points of $f(x,y)=4+x^3+y^3-3xy$.
\pause
Start with
\begin{equation*}
  \frac{\partial\, f}{\partial x}(x,y)=3x^2-3y
  \quad
  \frac{\partial\, f}{\partial y}(x,y)=3y^2-3x
\end{equation*}
and setting them to zero.
The first gives $y=x^2$.
Plugging into the second gives $3x(x^3-1)=0$, showing that $x=0$ or $x=1$.
With $y=x^2$ we have $(0,0)$ and $(1,1)$, and they both check.
\end{example}

\pause
\begin{definition}
A \alert{saddle point} $(a,b)$ is a critical point where in every 
neighborhood there is a point $(x,y)$ with $f(x,y)<f(a,b)$ and
there is also a point $(\hat{x},\hat{y})$ with  $f(x,y)>f(a,b)$.
\end{definition}
\end{frame}



\begin{frame}
  \frametitle{Second Derivative Test}

\begin{lemma}
Suppose that $(a,b)$ is a critical point of $\map{f}{\R^2}{\R}$
and that the second-order partials are continuous in some neighborhood
of $(a,b)$.
Call this $D$.
\begin{equation*}
  \frac{\partial^2\,f}{\partial x^2}\cdot \frac{\partial^2\,f}{\partial y^2}
     -\bigg(\frac{\partial^2\,f}{\partial x\partial y}\bigg)^2
\end{equation*}
Then
\begin{enumerate}
\item If at $(a,b)$ we have $D>0$ and $\partial^2f/\partial x^2>0$ 
   then $f$ has a relative minimum there.
\item If  at $(a,b)$ we have $D>0$ and $\partial^2f/\partial x^2<0$  at $(a,b)$ 
   then $f$ has a relative maximum there.
\item If at $(a,b)$ we have $D<0$ then $f$ has a saddle point there.
\end{enumerate}
\end{lemma}

If $D=0$ then the test gives no information: there are functions and points
with $D=0$ 
that have a max, there are functions that have a min, and there are functions
that have a saddle point.

\begin{example}
Above we found the critical points for
$f(x,y)=4+x^3+y^3-3xy$.
The second partials are here.
\begin{equation*}
  \frac{\partial^2\,f}{\partial x^2}=6x
  \qquad
  \frac{\partial^2\,f}{\partial y^2}=6y
  \qquad
  \frac{\partial^2\,f}{\partial x\partial y}=-3
\end{equation*}
Then $D=(6x)(6y)-(-3)^2=36xy-9$
and we have this.
\begin{center}
  \begin{tabular}{r|cr}
   \multicolumn{1}{c}{\textit{Critical point}}
    &\multicolumn{1}{c}{$D(a,b)$}
    &\multicolumn{1}{c}{\textit{Conclusion}}  \\
    \hline
    $(0,0)$ &$-9$   &saddle point  \\
    $(1,1)$ &$27$   &relative minimum  
  \end{tabular}
\end{center}
\end{example}

\end{frame}


\begin{frame}
\begin{exercise}
Find and classify the critical points for 
$f(x,y)=3x^2y+y^3-3x^2-3y^2+2$.    

\pause
The first partials are here.
\begin{equation*}
  \frac{\partial\,f}{\partial x}=6xy-6x
  \qquad
  \frac{\partial\,f}{\partial y}=3x^2+3y^2-6y
\end{equation*}
The second partials are here.
\begin{equation*}
  \frac{\partial^2\,f}{\partial x^2}=6y-6
  \qquad
  \frac{\partial^2\,f}{\partial y^2}=6y-6
  \qquad
  \frac{\partial^2\,f}{\partial x\partial y}=6x
\end{equation*}
The critical points for $x=0$ are $(0,0)$ and $(0,2)$.
The critical points for $y=1$ are $(1,1)$ and $(-1,1)$.
They classify, respectively, into a relative max, a relative min,
and two saddle points.
\end{exercise}
\end{frame}



\begin{frame}
  \frametitle{Absolute optima\Dash there is something about closed and bounded}

Consider the function $f(x)=1/x$ from Calculus~I
and the input interval $\open{0}{\infty}$.
On this interval the function does not attain
its minimum, since $f(x)$ keeps getting lower and lower as $x\to\infty$.
Also on this interval it does not attain its maximum, since
$f(x)$ keeps getting higher and higher as $x\to 0$.

But, if we change to an interval that is closed and bounded
such as $\closed{1}{10}$ then $f$ attains its minimum at the right
boundary point~$10$ and also attains its maximum at the left
boundary point~$1$.  

\pause
A key theorem in Calculus~I says that for any continuous function,
on an input interval that is closed and bounded
(that contains its endpoints and that is itself contained within
a circle), the function always attains its optima.
We can carry that result to 3D.

\begin{definition}
Let $R$ be a region in the plane~$\R^2\!$.
A point $(a,b)$ is on the \alert{boundary} of that region 
if every circular neighborhood 
of it contains both elements and non-elements of the region.
That is, every circle with center $(a,b)$, no matter how small its radius,
contains some $(x,y)\in R$ and some $(x,y)\notin R$.
A \alert{closed region} in $\R^2$ is one that contains all of its
boundary points.  
\end{definition}

\begin{definition}
A region~$R\subset\R^2$ is \alert{bounded} if there exists 
a circle centered at the
origin of large enough radius such that all of~$R$ lies inside the circle.
\end{definition}

\begin{theorem}
Let $\map{f}{\R^2}{\R}$ be continuous.
On a region that is closed and bounded, $f$ attains its optima.
That is, there is a point $(x_0,y_0)$ in the region where the function
is at a maximum and a point $(x_1,y_1)$ in the region where it is at a minimum. 
\end{theorem}
\end{frame}


\begin{frame}
  \frametitle{Finding absolute optima}

To find where the function has its optima,
find the critical points in the region and compute the value of the
  function at each and 
find the extrema of the function on the boundary. 
Then
the function's absolute maximum is the largest value of the function at any
of those points while
and its absolute minimum is the smallest.

\begin{example}
 On the rectangle $D=\set{(x,y)\suchthat 0\leq x\leq 3,\,0\leq y\leq 2}$
optimize the function 
$f(x,y)=2(x-1)^2+(y-1)^2=2x^2+y^2-4x-2y+3$.
\pause
First, the region is closed and bounded, and the function is continuous.
To find the critical points in~$D$ set the partials to zero.
\begin{equation*}
  \frac{\partial\,f}{\partial x}=4x-4=0
  \qquad
  \frac{\partial\,f}{\partial y}=2y-2=0
\end{equation*}
The only critical point is $(1,1)\in D$, and $f(1,1)=0$.

The boundary of $D$ has four line segments to check.
On the bottom where $y=0$ we have $f(x,0)=2x^2-4x+3$ for $0\leq x\leq 3$,
and optimizing this is a Calculus~I problem.
Working through it gives that the absolute minimum is that $f(1,0)=0$ and
the absolute maximum is $f(3,0)=9$.
Here are the rest.
\begin{center}
\begin{tabular}{rc|cc}
  \multicolumn{1}{c}{\textit{Side of $D$}} 
    &\multicolumn{1}{c}{\textit{Function}} 
    &\multicolumn{1}{c}{\textit{Minimum}}
    &\multicolumn{1}{c}{\textit{Maximum}}  \\
  \hline
  Bottom  &$f(x,0)=2x^2-4x+3$  &$f(1,0)=0$  &$f(3,0)=9$ \\  
  Right   &$f(3,y)=y^2-2y+9$   &$f(3,1)=8$  &$f(3,2)=9$ \\  
  Top     &$f(x,2)=2x^2-4x+3$  &$f(1,2)=1$  &$f(3,2)=9$ \\  
  Left    &$f(0,y)=y^2-2y+3$   &$f(0,1)=2$  &$f(0,2)=3$ \\  
\end{tabular}
\end{center}
\end{example}
\end{frame}

\begin{frame}
  \frametitle{On the boundary: Lagrange multipliers}

Recall that the level curves of a surface are like the altitude lines of
a topographic map.
\begin{center}
  \only<1>{\includegraphics[height=0.25\textheight]{topomap.png}}%
  \only<2->{\includegraphics[height=0.25\textheight]{topomap-region.png}}
\end{center}
\pause
We mark out some region on that map, and ask:~on
the border of that region then what is the function's max and min?

\pause
The key observation is that at a max or min, 
the border meets the level curve
such that the two normal vectors are parallel.


\begin{theorem}[3D version]
To optimize $f(x,y,z)$ subject to
constraint $g(x,y,z)=k$ for some $k\in\R$, 
get the candidate $(x,y,z)$'s by solving this system
(where $\del g\neq\vec{0}$)
\begin{equation*}
  \del f(x,y,z)=\lambda\cdot\del g(x,y,z)
  \quad\text{and}\quad
  g(x,y,z)=k
\end{equation*}
where $\lambda\in\R$ is the \alert{Lagrange multiplier}.
\end{theorem}
\end{frame}

\begin{frame}
\begin{example}
Optimize $f(x,y)=x^2-2y$ subject to $g(x,y)=x^2+y^2=9$.    
\pause We have
\begin{equation*}
\colvec{2x \\ -2}=\lambda\cdot\colvec{2x \\ 2y}  
\end{equation*}
giving this system.
\begin{align*}
  2x      &= 2\lambda x  \\
  -2      &= -2y\lambda  \\
  x^2+y^2 &= 9
\end{align*}
The first equation $x(1-\lambda)=0$ gives the cases $x=0$ or $\lambda=1$.
If $x=0$ then the third equation says $y=\pm 3$.
If $\lambda=1$ then the the second equation $\lambda y+1=0$ gives $y=-1$ and then
the third equation gives $x=\pm 2\sqrt{2}$.
\begin{center}
  \begin{tabular}{r|*{4}{c}}
    \textit{$(x,y)$} 
         &$(0,-3)$ &$(0,-3)$ &$(-2\sqrt{2},-1)$ &$(2\sqrt{2},-1)$  \\
    \cline{2-5}
    \textit{$f(x,y)$} 
         &$6$      &$-6$     &$10$              &$10$
  \end{tabular}  
  \qquad\pause
  \only<3->{\vcenteredhbox{\includegraphics[height=0.35\textheight]{asy/lagrange000.pdf}}}
\end{center}

\end{example}
\end{frame}


\begin{frame}
\begin{exercise}
Postal regulations limit boxes to 
$2\cdot\text{width}+2\cdot\text{height}+\text{length}\leq 108$.
Maximize volume.

\pause
We maximize $V(x,y,z)=xyz$ subject to $g(x,y,z)=x+2y+2z=108$.
Here are the Lagrange equations.
\begin{equation*}
  \colvec{yz \\ xz \\ xy}=\lambda\cdot\colvec{1 \\ 2 \\ 2}
  \qquad
  x+2y+2z=108
\end{equation*}
With $\lambda=yz$, substitute into the second line to get $z(x-2y)=0$.
Obviously $z\neq 0$ so $x=2y$.
Next substitute $\lambda=yz$ into the third line, giving
$y(x-2z)=0$.
With $y\neq 0$ we get $x=2z$.
Thus $2y=x=2z$ and then $y=z$.
Drop these into the constraint 
$2y+2y+2y=108$ and solving gives $y=18$, and $z=18$, and $x=36$.  
\end{exercise}
\end{frame}

\end{document}
%%% Local Variables: 
%%% coding: utf-8
%%% mode: latex
%%% TeX-engine: luatex
%%% End: 







