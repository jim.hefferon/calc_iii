\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}

% \usepackage[perpage,symbol*,para,multiple]{footmisc}
% Getting undefined commands in aux file
\makeatletter
\newcommand\FN@pp@footnote@aux[2]{\relax}
\makeatother
\usepackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 

% set the fonts
\usefonttheme{professionalfonts} % using non standard fonts for beamer
\usefonttheme{serif} % default family is serif
\usepackage{../../presentation,../../presentationfonts}
% \usepackage{jh}
% \newcommand{\colvec}[1]{%
%   \begin{pmatrix} #1 \end{pmatrix}}
\usepackage{../calciii}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Directional derivative}

\author{Jim Hef{}feron}
\institute{
  Mathematics and Statistics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{hefferon.net}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Directional derivative}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}




% ------------------------------------------------------
\begin{frame}
  \frametitle{Where we are}

A derivative is a rate of change.

We can approximate how much change a function undergoes in
a given direction.
\begin{align*}
  dz 
  &=\frac{\partial z}{\partial x}\cdot dx
    +\frac{\partial z}{\partial y}\cdot dy  \\
  \Delta z
  &\approx\frac{\partial z}{\partial x}\cdot \Delta x
    +\frac{\partial z}{\partial y}\cdot \Delta y
\end{align*}
The approximation improves when the changes $\Delta x$ and $\Delta y$
are close to $0$.
The first line idealizes that to infinitesimal changes.

\begin{example}
Let $z=4-2x^2-y^2$, so that $\partial z/\partial x=-4x$ 
and $\partial z/\partial y=-2y$.
Stand at $(x,y)=(1,1)$ and look up, so $z=1$.
Above our heads, the slope parallel to the $x$~axis is 
$\partial z/\partial x=-4$, 
while the slope parallel to the $y$~axis is $\partial z/\partial y=-2$.

If we move diagonally, 
by $\Delta x=0.1$ and $\Delta y=0.1$, then the 
approximate resulting change is 
$\Delta z=(-4)\cdot (0.1)+(-2)\cdot (0.1)=-0.6$.
If we move in the same direction but more, 
by $\Delta x=0.2$ and $\Delta y=0.2$, then the 
approximate resulting change is 
$\Delta z=(-4)\cdot (0.2)+(-2)\cdot (0.2)=-1.2$.
\end{example}

So although we can express the change, we can't express the rate of change.
\end{frame}

\begin{frame}
  \frametitle{Directional derivative}
\begin{definition}
Fix a unit direction vector $\vec{u}=\langle a,b\rangle$.
For a function $\map{f}{\R^2}{\R}$,
The \alert{directional derivative of $f$ in the direction of $\vec{u}$}
is
\begin{equation*}
  D_{\vec{u}}f(x_0,y_0)
  =
  \lim_{h\to 0}\frac{f(x_0+ha,y_0+hb)-f(x_0,y_0)}{h}
\end{equation*}
provided that limit exists.
\end{definition}

\pause
The unit vectors in the $x$~and $y$~axes give these familiar cases.
\begin{equation*}
  D_{\vec{i}}\,f=\frac{\partial \,f}{\partial x}
  \qquad
  D_{\vec{j}}\,f=\frac{\partial \,f}{\partial y}
\end{equation*}

\begin{lemma}
We usually compute the directional derivative as a linear combination;
here are the 2D and 3D versions:
\begin{align*}
  D_{\vec{u}}\,f(x,y)
  &=\frac{\partial f}{\partial x}(x,y)\cdot a
  +\frac{\partial f}{\partial y}(x,y)\cdot b    \\
  D_{\vec{u}}\,f(x,y,z)
    &=\frac{\partial\,f}{\partial x}(x,y,z)\cdot a
    +\frac{\partial\,f}{\partial y}(x,y,z)\cdot b
    +\frac{\partial\,f}{\partial z}(x,y,z)\cdot c
\end{align*}
where $\vec{u}=a\vec{i}+b\vec{j}$ or 
$\vec{u}=a\vec{i}+b\vec{j}+c\vec{k}$.
\end{lemma}
For instance, in 2D it says that $f$'s rate of change in the direction given by 
$\vec{u}$ is $f$'s rate of change in the $x$-direction times the
part of $\vec{u}$ that is in the $x$-direction, 
plus $f$'s rate of change in the $y$-direction times the
part of $\vec{u}$ that is in the $y$-direction
\end{frame}


\begin{frame}
The prior slide says that
the directional derivative is a linear combination
\begin{align*}
  D_{\vec{u}}\,f(x,y)
  &=\frac{\partial f}{\partial x}(x,y)\cdot a
  +\frac{\partial f}{\partial y}(x,y)\cdot b    \\
  D_{\vec{u}}\,f(x,y,z)
    &=\frac{\partial\,f}{\partial x}(x,y,z)\cdot a
    +\frac{\partial\,f}{\partial y}(x,y,z)\cdot b
    +\frac{\partial\,f}{\partial z}(x,y,z)\cdot c
\end{align*}
where $\vec{u}=a\vec{i}+b\vec{j}$ or 
$\vec{u}=a\vec{i}+b\vec{j}+c\vec{k}$.


\begin{example}
  Let $f(x,y)=4-2x^2-y^2$.
% We have this.
% \begin{equation*}
%   D_{\vec{u}}\,f(x,y)
%   =
%   =\frac{\partial f}{\partial x}(x,y)\cdot a
%   +\frac{\partial f}{\partial y}(x,y)\cdot b
% \end{equation*}
In the direction of the unit vector
$\vec{u}=(1/2)\vec{i}+(3/2)\vec{j}$, the above equation gives this.
\begin{equation*}
  D_{\vec{u}}\,f(x,y)
  =-4x\cdot \frac{1}{2}
  -2y\cdot \frac{\sqrt{3}}{2}
\end{equation*}
At $(x,y)=(1,1)$, we get this.
\begin{equation*}
  D_{\vec{u}}\,f(1,1)=-(2+\sqrt{3})
\end{equation*}
\end{example}

\begin{exercise}
Let $f(x,y)=x\cdot\cos(2y)$.
We will find the directional derivative at $(0,\pi/4)$ in the 
direction of $\vec{v}=2\vec{i}+3\vec{j}$.
\begin{enumerate}
\item Find the unit vector in the direction of $\vec{v}$.
\item Compute the directional derivative by using the linear combination
formula.
\item Find the directional derivative at $(0,\pi/4)$.
\item Sketch.
\end{enumerate}
\end{exercise}
\end{frame}



\begin{frame}
  \frametitle{Gradient}

The directional derivative is a linear combination:
$D_{\vec{u}}\,f(x,y)=\frac{\partial f}{\partial x}(x,y)\cdot a
  +\frac{\partial f}{\partial y}(x,y)\cdot b$ or
$D_{\vec{u}}\,f(x,y,z)
    =\frac{\partial\,f}{\partial x}(x,y,z)\cdot a
    +\frac{\partial\,f}{\partial y}(x,y,z)\cdot b
    +\frac{\partial\,f}{\partial z}(x,y,z)\cdot c$
where $\vec{u}=a\vec{i}+b\vec{j}$ or 
$\vec{u}=a\vec{i}+b\vec{j}+c\vec{k}$.
Rewrite it as a dot product.
\begin{equation*}
  \colvec{\frac{\partial f}{\partial x}(x,y) \\[1.25ex] 
          \frac{\partial f}{\partial y}(x,y)}
  \dotprod
  \colvec{a \\ b}
  =
  \colvec{\frac{\partial f}{\partial x}(x,y) \\[1.25ex] 
          \frac{\partial f}{\partial y}(x,y)}
  \dotprod
  \vec{u}
\end{equation*}

\pause
\begin{definition}
Let $\map{f}{\R^2}{\R}$.
The \alert{gradient} of $f$ is
\begin{equation*}
  \del\,f=\colvec{\partial f/\partial x \\ \partial f/\partial y}
  \quad\text{or}\quad
  \del\,f=\colvec{\partial f/\partial x \\ 
                  \partial f/\partial y  \\ 
                  \partial f/\partial z}
\end{equation*}
(read it aloud as ``del $f$'').
\end{definition}

Thus we don't need different versions for 2D, 3D, 
etc.: $D_{\vec{u}}\,f=\del f\dotprod \vec{u}$.

\begin{exercise}
 Find $\del f$ where $f(x,y,z)=xz^2+x\cos(2y)$
\end{exercise}
\end{frame}


\begin{frame}
  \frametitle{Geometric significance of the gradient}

Geometrically, a dot product depends on the angle~$\theta$ between the 
vectors.
\begin{equation*}
  \del f\dotprod\vec{u}
  =
  |\del f|\cdot |\vec{u}|\cdot\cos(\theta)
  =|\del f|\cdot\cos(\theta)
\end{equation*}

\begin{lemma}
The maximum value of $D_{\vec{u}}(f(x,y))$,
the maximum rate of increase of the function $f(x,y)$ 
is $|\del f(x,y)|$ and occurs in the direction of $\del f(x,y)$.
The maximum rate of decrease the negative of that 
and occurs in the opposite direction.
(The same holds in 3D, etc.)
\end{lemma}

\pause
\begin{example}
Consider the paraboloid $z=4+x^2+3y^2$.
At the point $(x,y,z)=(2,-1/2,35/4)$, what direction achieves steepest ascent?
Descent?

We have
\begin{equation*}
  \del f=\colvec{2x \\ 6y} 
  \quad\text{so}\quad
  \del f(2,-1/2)=\colvec{4 \\ -3} 
\end{equation*}
and so the steepest ascent is in the direction of 
$\vec{v}=4\vec{i}-3\vec{j}$.
Steepest descent is in the direction of  
$-\vec{v}=-4\vec{i}+3\vec{j}$.
\end{example}

\begin{exercise}
\begin{enumerate}
\item For the same function, what is the direction of steepest ascent at
$(3,1,16)$?
\item In what direction is $f$'s rate of change zero?
(Hint: make the dot product zero.)
\end{enumerate}
\end{exercise}
\end{frame}


\begin{frame}
  \frametitle{Properties of the gradient}
\begin{itemize}
\item $\del (f+g)=\del f+\del g$
 and
  $\del (c\cdot f)=c\cdot \del f$  for a constant $c\in\R$
\item Product Rule:
  $\del (fg)=f\cdot \del g+g\cdot \del f$
\item Chain Rule: 
  Where $g$ is a differentiable function of one variable,
  $\del g(f(x,y,z))=g^\prime(f(x,y,z))\cdot \del f$
\end{itemize}
\end{frame}

\end{document}







