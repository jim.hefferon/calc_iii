\documentclass[9pt,t,serif,professionalfont,xcolor={table,dvipsnames},noamsthm]{beamer}

% \usepackage[perpage,symbol*,para,multiple]{footmisc}
% Getting undefined commands in aux file
\makeatletter
\newcommand\FN@pp@footnote@aux[2]{\relax}
\makeatother
\usepackage{xr-hyper}  % for cleveref; https://tex.stackexchange.com/a/244272/121234
\PassOptionsToPackage{pdfpagemode=FullScreen}{hyperref}
\setbeamersize{text margin left = 0.5cm,
               text margin right = 0.3cm}
\mode<presentation>
{
  \usetheme{boxes}
  \setbeamercovered{invisible}
  \setbeamertemplate{navigation symbols}{} 
}
\hypersetup{colorlinks=true,linkcolor=blue} 

% set the fonts
\usefonttheme{professionalfonts} % using non standard fonts for beamer
\usefonttheme{serif} % default family is serif
\usepackage{../../presentation,../../presentationfonts}
% \usepackage{jh}
% \newcommand{\colvec}[1]{%
%   \begin{pmatrix} #1 \end{pmatrix}}
\usepackage{../calciii}

% load the pdfanim style, should be done after hyperref
% \usepackage{pdfanim}

% load and initialise animation
% arguments:
%   \PDFAnimLoad[options]{name}{xxx}{number}
%   - options: in this example the animation is looped
%   - name: name of the animation
%     (with this name it can be reused several times in the document)
%   - number: number of animation frames / files (n)
% the animation will be composed of files
% xxx0.pdf, xxx1.pdf ... xxx(n-1).pdf


\title{Partial Derivatives}

\author{Jim Hef{}feron}
\institute{
  Mathematics and Statistics\\
  Saint Michael's College\\
  Colchester, VT\\[1ex]
  \texttt{hefferon.net}
}
\date{}

% This is only inserted into the PDF information catalog.
\subject{Partial Derivatives}

\begin{document}
\begin{frame}
  \titlepage
\end{frame}




\section{Partial Derivatives}
% ------------------------------------------------------
\begin{frame}
  \frametitle{How can we find rates of change when there is more than one variable?}

For a function $f$ of one variable,
the derivative $df/dx$
gives the rate of change of the function as its input $x$
changes. 
What to do with functions of multiple variables, $\map{f}{\R^n}{\R}$? 

We will ask what happens when one variable changes but the others remain 
constant.
(We will see below that if we want more than one variable to change then we
will handle that as a combination of one-at-a-time's.)


\begin{example}
Let $z = f(x,y) = 4-2x^2-y^2$.
This is an elliptic paraboloid.  

Start by fixing $y=0$ but letting $x$ vary.
The geometry is that we are looking at the intersection of the parabolid with 
the $xz$~plane.
With $y=0$ we describe the curve or intersection with
$z=4-2x^2-0^2=4-2x^2\!$.
We know that the rate of change of the function given by this expression
is $-4x$.
For instance, the point $(1,0,2)$ is on the surface,
and the slope of the line in the $xz$ plane is $-4$.

Similarly, we can fix $x=1$ and then the curve of intersection 
of the paraboloid with the
plane $x=1$ is given by the formula
$z=4-2-y^2=2-y^2\!$.
Its rate of change is given by $-2y$.
\end{example}

\pause
\begin{example}
Let $f(x,y)=4xy^3$.
We will fix~$y$ but allow $x$ to vary, and compute how fast $f$ changing
at the point $(x,y)=(2,3)$. 

Because 
$y$ is fixed, $4xy^3$ is a one-variable function, as in Calc~I.
The derivative is $4y^3$.
Then $f$'s rate of change is $4\cdot 27=108$
when we stand at $(x,y)=(2,3)$ and head in the direction of this vector.
\begin{equation*}
  \colvec{1 \\ 0}
\end{equation*}
% sage: x, y = var('x y')
% sage: plot3d(2*x * y^3, (x, -1, 1), (y, 0, 4))

% We could also fix the first variable at $x=a$
% and allow $y$ to vary. 
% Then we have $2a\cdot 2y^3\!$, whose derivative is 
% $12a\cdot y^2\!$.
\end{example}
\end{frame}



\begin{frame}
\frametitle{Definition of partial derivative}  

\begin{definition}
The \definend{partial derivative of $f(x,y)$ with respect to $x$} is
\begin{equation*}
 \frac{\partial\, f}{\partial x}= \lim_{h\to 0}\frac{f(x+h,y)-f(x,y)}{h}
\end{equation*}
(provided that the limit exists).
It is also sometimes denoted $f_x$.
Similarly, the \definend{partial derivative of $f(x,y)$ with respect to $y$} is
$\lim_{h\to 0}[f(x,y+h)-f(x,y)]/h$
(provided that it exists)
and is denoted $\partial f/\partial y$, or sometimes $f_y$.
\end{definition}

\begin{exercise}
Find all of the partial derivatives of these.
\begin{enumerate}
\item $f(x,y)=x^4+6\sqrt{y}-10$
\item $w=w(x,y,z)=x^2y-10y^2z^3$
% \item $h(s,y)=t^7\ln(s^2)+(9/t^3)-\sqrt[7]{s^4}$
% \item $f(x,y)=\cos(4/x)\cdot e^{x^2y-5y^3}$
\end{enumerate}
\end{exercise}
\end{frame}


\begin{frame}
\frametitle{Higher-order derivatives}

We can take the derivative of the derivative.
Here is the notation for two of the four.
\begin{equation*}
  \frac{\partial \,\frac{\partial\, f}{\partial x}}{\partial x}
  =\frac{\partial^2\,f}{\partial x^2}
  \qquad
  \frac{\partial \,\frac{\partial\, f}{\partial x}}{\partial x}
  =\frac{\partial^2\,f}{\partial x^2}
\end{equation*}
These are sometimes denoted $f_{xx}$ and $f_{yy}$.
Here are the mixed partials.
\begin{equation*}
  \frac{\partial \,\frac{\partial\, f}{\partial y}}{\partial x}
  =\frac{\partial^2\,f}{\partial x\partial y}
  \qquad
  \frac{\partial \,\frac{\partial\, f}{\partial x}}{\partial y}
  =\frac{\partial^2\,f}{\partial y\partial x}
\end{equation*}
These are sometimes denoted $f_{yx}$ and $f_{xy}$; note the reversal of
the subscript orders.
It comes from thinking of the first as $(f_y)_x$ and of the second 
as $(f_x)_y$.

\begin{exercise}
Find all four higher order partials of
% \begin{enumerate}
% \item 
$f(x,y)=x^3+xe^{-3y}$.
% \item $g(x,y)=x^2y^3+\sin(2x-5y)$
% \item $h(s,t)=\cos(s^2+5)-s^4t^3+\ln(t^{-2})$
% \end{enumerate}
\end{exercise}

\pause
\begin{theorem}[Clairaut’s Theorem]
Let the function $f(x,y)$ be defined on an open disc $D$ that contains the
point $(a,b)$.
Also suppose that
% the first order partials $\partial f/\partial x$ and $\partial f/\partial y$
% and 
the mixed second order partials $\partial^2f/\partial x\partial y$
and $\partial^2f/\partial y\partial x$ are continuous on~$D$.
Then those mixed partials are equal.
\end{theorem}
\end{frame}



\begin{frame}
  \frametitle{Implicit differentiation}
Recall from Calc~I that sometimes a variable is not defined explicitly
as a function of the others, but instead is defined implicitly.
\begin{example}
Where $3y^4+x^7=5x$, to find $dy/dx$,
first think of $y$ as a function of~$x$.
\begin{equation*}
  3[y(x)]^4+x^7=5x
\end{equation*}
Differentiate with respect to~$x$,
remembering to apply the Chain Rule.
\begin{equation*}
  12[y(x)]^3\,\frac{dy}{dx}+7x^6=5
\end{equation*}
Finish by solving for $dy/dx$.
\begin{equation*}
  \frac{dy}{dx}=\frac{5-7x^6}{12y^3}
\end{equation*}

\begin{example}
Where  $x^3z^2-5xy^5z=x^2+y^3$, find $\partial z/\partial x$.

Start by writing $x^3[z(x,y)]^2-5xy^5z(x,y)=x^2+y^3$.
Take the partial with respect to~$x$. 
Note that in the first term,
for instance, you need the Product Rule as both $x$ and $z(x,y)$
are functions of~$x$.
\begin{equation*}
  3x^2[z(x,y)]^2+2x^3[z(x,y)]\frac{\partial z}{\partial x}
    -5y^5z(x,y)-5xy^5\frac{\partial z}{\partial x}=2x
\end{equation*}
Now solve for $\partial z/\partial x$.
\begin{equation*}
  \frac{\partial z}{\partial x}=\frac{3y^2+25xy^4z}{2x^3z-5xy^5}
\end{equation*}
\end{example}
\end{example}
 
\end{frame}


\end{document}

\begin{frame}

\end{frame}

\begin{frame}
  
\end{frame}


\end{document}






